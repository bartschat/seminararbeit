\cleardoublepage
\pagenumbering{arabic}
\setcounter{page}{1}

# Introduction

* Starting with a bold statement about the impact and size of video games\cite{Board2016}
* introduce the reader to the video games market: 
	 * sales volumes/revenues\cite{EntertainmentSoftwareAssociation2016}
	 * comparison to movie/music markets
	 * time spent gaming
* define requirements for gaming: gaming platform, games and tv
* gaming platform according \cite{Marchand2013}
* statement: need to define video game platform in detail - zoom into the framework
* discuss the question: Which ingredients turn a video game console into a successful platform according the current state of scientific research? 
\begin{figure}
	\includegraphics[scale=0.65]{Images/Framework_Entwicklung_1}
    \caption{Framework Development Step I}
    \label{abb:framework1}
\end{figure}

## Closer look at the video games market

* video games are a hedonic \cite{Hirschman1982}product, what does that mean for consumer´s behaviour?
* may be we can tranfer success factors from other hedonic markets, e.g. movies? \cite{Hennig-Thurau2006a}\cite{Hennig-Thurau2006}
* conclusion: the consumer buys an "experience" and evaluates products according their ability to deliver desirable experiences

\begin{figure}
	\includegraphics[scale=0.65]{Images/Framework_Entwicklung_2}
    \caption{Framework Development Step II}
    \label{abb:framework2}
\end{figure}

## Producers positioning their platforms for different target audiences

* assumption: the game console producers position their brands and their product to target specific audiences\cite{Kuo2016}
* in gaming: casual gamer, core gamer, hardcore gamer, maybe more?
* define gamer personas and determine what kind of experiences they are looking for in game consoles\cite{Scharkow2015}
* common factor: all gamers are looking for enjoyment \cite{Sweetser2005} and immersion \cite{Jennett2008}\cite{Brown2004}

## What´s immersion in video games and how is it achieved? 

* need for powerful hardware for realistic graphic and sound effects
* need for hardware that´s designed for the target audience, especially the controller, because it is THE point of interaction with the console/game
* the fitting software for the gamer type, e.g. the right game 
* the correct amount of social activity (online services, multiplayer gaming, leading to direct network effects)\cite{Hsu2004}

## Framework suggestion

Trying to answer the question "Which ingredients turn a video game console into a successful platform according the current state of scientific research?" with the help of a proposed framework according to figure \ref{abb:framework3}.

\begin{figure}
	\includegraphics[scale=0.65]{Images/Framework_Entwicklung_3}
    \caption{Draft of a comprehensive framework for the determination of success factors for video game platforms}
    \label{abb:framework3}
\end{figure}

* additional assumption that a successful gaming platform needs not only the right software/games, but also a variety/quantity of games available, because a console without games is of little use (leading to indirect network effects)


# Hardware

to achieve immersion we need performance and design

## Performance factors

* immersion via telepresence
* short view on hardware performance development of different generations

## Design factors, focussing on controller design

* show different controller design
* connect controller designs with gamer personas, evolving over time from general audience with simple controllers in the 70s and 80s, to hitech controller like PS3, XBox 360 and in contrast the Wiimote and other motion based controllers leading to a shift in the spatial experience
... \cite{Stuart2016}
... \cite{Kelechava2015}
... \cite{Wingrave2010}
... \cite{Lu2003}


# Software

## Quality of Software

* effects of superstar software as system seller\cite{Binken2009}
* short analysis of the launch lineups of a choice of platforms (maybe this is better put into the application part?!)

## Quantity/variety of Software

* indirect network effects\cite{Cenamor2013}\cite{Clements2005}\cite{Shankar2002}

# Services

## Multiplayer Gaming

* social gaming, competetive gaming\cite{Domahidi2014}\cite{Hsu2004}
* direct network effects\cite{Marchand2016}

## Social factors 

* social experiences
* flow in social gaming
* social interaction via chat, voice \cite{Trepte2012}
* self-expression (achievments, gamer score, ...)

## Digital game distribution

* use for consumer: ease of consumption
* use for producer: direct sales, no logistics, vendor lock in - switching costs

## Technical requirements

* stability
* responsiveness
* ease of use, User Interface Design


# Application of the framework on n exemplary video game platforms

## Nintendo Wii

* targetting/positioned for casual gamers and former non-gamers
* controller design, motion control
* reduced complexity


## Nintendo Wii U

* still targetting casual gamers
* controller layout too complex
* too little differentiation against predecessor, especially in naming!

## The Apple iPhone

* is it a gaming device? come up with numbers, e.g. AppAnnie
* apply framework and show why it´s a successful gaming system
* simple controller design = direct input via touch
* huge game quantity = variety
* lot of high quality games = show via metacritic
* discuss if there are any superstar software title

# Discussion, limitations and further research 

tbd
