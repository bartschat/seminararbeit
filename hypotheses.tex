\nonstopmode

\documentclass
[
    %twocolumn,
    a4paper,
    %german,
    %twoside,
    10pt
]
{article}
\usepackage[utf8]{inputenc}
\usepackage{float}
%\usepackage[ngerman]{babel}
\usepackage{enumerate}
\usepackage[bottom]{footmisc}
\usepackage{array}
\usepackage{ntheorem}
\usepackage{parskip}
\usepackage[right]{eurosym}
\usepackage{xcolor}
%\usepackage[hyphens]{url}
\usepackage{url}
\usepackage{makeidx}
\usepackage{multicol}
\usepackage{theorem}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{pgfplots}
\pgfplotsset{compat=1.5}
\usepackage{csvsimple}
\usepackage{fancyhdr}
\usepackage{colortbl}
\usepackage{bchart}
\usepackage{amssymb}
\usepackage{pdflscape}
\usepackage{soul}
\usepackage{setspace}
%\usepackage{showframe}
\usepackage[left=4.0cm,right=2.5cm,top=2.5cm,bottom=2.5cm]{geometry}
\pagestyle{plain}
\rhead{\thepage}
\sloppy

\setlength{\unitlength}{1cm}
\setlength{\oddsidemargin}{0.3cm}
\setlength{\evensidemargin}{0.3cm}
\setlength{\textwidth}{15.5cm}
\setlength{\topmargin}{-1.2cm}
\setlength{\textheight}{24.7cm}
\columnsep 0.5cm

\title{Outline of the Master's Thesis - Video Games: Motives and Barriers}
\selectcolormodel{gray}
\begin{document}
%\onehalfspacing

\begin{figure}[ht!]
    \centering
    \includegraphics[scale=0.25]{Images/FrameworkDraft}
    \caption{Conceptual Model - Video Games: Motives and Barriers}
    \label{fig:framework}
\end{figure}

\section{Hypotheses}

\subsection{General Hypotheses}

\begin{enumerate}
\item According to \cite{Pardee1990} a motive is the reason of an individual to act in a certain way or to lean towards a certain behavior. Consequently I argue, that the listed motives for video games use are valid reasons that will determine the frequency and duration of video game use. The distinctiveness of the motives shall be proportional to the actual use.
\item If a barrier is defined as something that impedes or separates \cite{MerriamWebsterBarrier2017}, then a barrier against the use of video games impedes the act of gaming. Therefore I propose that the absence of barriers or the presence and overcoming of barriers is a necessary but not sufficient precondition for the use of video games, i.e. the use of video games (frequence and duration) will be antiproportional to the distinctiveness of the individual's barriers.
\item In case of the Non-Gamer there has to be at least one barrier being significant enough to prevent video game use at all or all motives have to be insignificant enough.
\end{enumerate}

\subsection{Motive Hypotheses}
\begin{enumerate}
\item Sociability or social connectedness is one of the basic human psychological needs. This has been established by \cite{Maslow1943} and been reiterated in the gaming context by \cite{Ryan2006} as the need for relatedness. As certain video games are assessed to be able to satisfy this need, \textbf{sociability} is a motive for the use of video games. \cite{Kaye2012}
\item Obtaining the feeling of accomplishment or achievement has been identified as a psychological need and consequently as a motivating factor, e.g. \cite{McClelland1987}'s power and achievement motivations, which show that power or dominance are effective motives. As video games can be a mean to achieve the status of gaining power or dominance over others in a competitive setting, I propose that \textbf{competition} is a motive for video games use.
\item Video games offer the chance to avoid real life obligations and issues and blank out negative emotions, feelings or experiences. I argue, that \textbf{escapism} can be a motive for video games use, because games are used as a means to achieve this blanked out state. Contrasted to the motive of altering emotional states, to which it could be linked, I consider escapism to be a masking of tangible or intangible issues, whereas altering emotional states tries to change the actual emotional condition.
\item In line with the mood management theory \cite{Zillmann1988} and the "altered states" approach by \cite{Lazzaro2004}, I conclude that reaching another, more positively assessed, emotional state is a desired objective. When video games can be deployed to achieve this state, the wish to \textbf{alter emotional states} is a motive for video games use. In contrast to the escapism motive, to which altering emotional states could be related to, it focusses on changing the actual emotional condition of the individual.
\item \cite{Cheyne2006} defines three types of boredom: being prevented from doing what someone wants to do, being forced to do something one does not want to do or being free to do what one wants but being unable to maintain interest or attention to any mental or environmental object. As video games can be used to counter all three types of boredom, which is deemed as an unpleasant feeling \cite{Koerth-Baker2016}, I propose that \textbf{time killing} is a motive for video games use.
\item Because \textbf{immersion} is a pleasurable effect in itself \cite{Murray1999}, gamers are motivated to use video games, to experience this feeling of pleasure.
\item From the fact, that achieving efficacy is a gratifying experience, stems the immediate motivation for the individual to deal with its environment, be it the real world or the game world. Due to the rewarding nature of this experience it is a motive to continue the "active exchange with his or her environment". This is a motive for the use of video games because video games establish the player as the causal agent, responsible for change in the game world, by interactively reacting to every single player input. \cite{Klimmt2006} It is important to mention that \textbf{effectance} is not considered intentionally in the decision towards video game use but rather that the absence of effectance hinders it. \cite{Klimmt2006}
\item As \cite{McClelland1987} explains, achievement is a motivation for human actions. The feeling of accomplishment is positivily connoted. As the use of video games can lead to gaining this feeling, \textbf{achievement} is a motive for it as \cite{Lazzaro2004} describes in her "Hard Fun" construct.
\item As \cite{Stewart2011} states that "understanding is its own reward" one can assume that \textbf{exploration} is a motive for the use of video games to achieve this gratifying experience of understanding.
\end{enumerate}

\subsection{Barrier Hypotheses}
\begin{enumerate}
\item Unpleasing aesthetic sensations regarding a game, i.e. a misfit between actual and preferred game aesthetics, lead to the expression of taste in the form of rejection. Therefore \textbf{aesthetics} are a barrier for the use of video games. Taste as a barrier can be identified in the works of \cite{Gandasegui2010, Juul2009, Scharkow2015}.
\item Violating or acting against the morality or ethical values of the individual gamer means to conduct an act that is determined as wrong by him. If a game contains elements that are evaluated as immoral or unethical this leads to a rejection of those elements or the game as a whole by the individual. Therefore, \textbf{moral reasons} are a barrier against video game use.
\item The intended use of video games depends on the accessibility of the necessary ressources, i.e. a device which is able to run the games software. A lack of this ressource leads to the nonfulfillment of a necessary condition for the use of video games. Consequently, an \textbf{inaccessibility of game devices} is a barrier against video games use. This also includes the case of having a gaming device that is not able to run the desired software, e.g. due to hardware requirements.
\item Using video games requires the accessibility of games software, which is able to be used on the available game device. A lack of the ressource "game" is a nonfulfillment of a necessary condition for the use of video games, which leads to the hypothesis, that the \textbf{inaccessibility of games} is a barrier against video games use.
\item Video games demand financial ressources for their acquisition and/or their use in various forms, acquisition price and/or subscription fees, in game purchases. When the demand exceeds the financial ressources available for acquiring games/game devices this lack of ressource is a barrier against video game use. When the demand can be met with the available financial funding, but the projected/assumed value of the gaming experience, which cannot be determined prior purchase/use due to games being experience goods \cite{Nelson1970}, is not adequate to the asked price, this results in an unwillingness to pay of the customer \cite{Homburg2005, Simon2017}, again establishing \textbf{costs} as a barrier.
\item Video games demand the assignment of ressources for their use, including the assignment of uninterrupted time slots of a certain length, which is defined by the game design. When the time demand exceeds the amount of available time ressources \textbf{time constraints}, as a lack of ressources, are a barrier against video game use.
\item High complexity in a game leads to a high cognitive load due to the necessity of learning processes in the gamer. This high cognitive load is compared to the potential value of the experience provided by using the game. If the cognitive efforts are assessed as to high for the expected outcome, \textbf{complexity} will act as a barrier against video game use. Complexity will have to be assessed in relation to the individual's experience with gaming, as it was shown that the brain adapts to video gaming. \cite{Kuehn2014}
\item Video games are experience goods, that means the individual is unable to determine the total value and cost before purchasing and using the good. \cite{Nelson1970} As video game illiterates have no positive experience with gaming, they will most likely not associate a rewarding experience as value with video games use, so that \textbf{unfamiliarity} with gaming is a barrier against video game use.
\item As the number of video games released and available for purchase and consumption is rising steadily, according to \cite{SteamSpy2017} the digital games platform Steam had more than 17,000 titles available in 2017, this number creates the "too much choice effect" \cite{Iyengar2000} that "can indeed lead to negative consequences such as dissatisfaction, regret,  disappointment, decreased motivation to make a choice, or decreased consumption rates" \cite{Scheibehenne2008} regarding video games. I propose that having too much choice or \textbf{hyperchoice} is a barrier against the use of video games, as it imposes the mentionend negative consequences to the medium "digital games".
\end{enumerate}

\subsection{Gamer Type Hypotheses}

Core Gamer - Achievement - Speedruns?!

\subsection{Game Type Hypotheses}

As there are different criteria for games to be considered as either core game or casual game, these might have different effects on motives and barriers regarding the general use of video games and their specific use by an individual. Especially Casual Games could have mitigating effects on some of the barriers, while Core Games could have reinforcing or intesifying effects on some of the motives.

\begin{tabular}{ | l | l | l | }\hline
  Criterion             & Core Game   & Casual Game \\ \hline
  Accessibility         & low         & high \\
  Acceptability         & low         & high \\
  Usability             & low         & high \\
  Simplicity            & low         & high \\
  Flexibility           & low         & high \\
  Interruptibility      & low         & high \\
  Difficulty, initial.  & low         & high \\
  Punishment            & severe      & easy \\
  Juiciness             & high, diegetic & high, non-dieg. \\
  \hline
\end{tabular}

\subsubsection{Hypotheses regarding the effects of Core Games on Motives for Video Game Use}

\begin{itemize}
\item Sociability - Social gaming is relatively common in the Core Games segment, as genres like the MMORPG, MOBA and Team-FPS exemplify. As there was no concrete indicator for an effect of Core Games on the "sociability" motive I cannot propose a hypothesis. This indifference is supported by the works of \cite{Juul2010}, who introduced three considerations that a player has to balance in multiplayer games: goal orientation, game experience and social management. Core Games offer the chance to focus on any combination of them, as well as Casual Games do, therefore I cannot presume any effect of Core Games on the "sociability" motive for the use of video games.
\item Competition - As competition can be identified as one of the main drivers of play in Core Games like World of Warcraft \cite{Billieux2013} the element of what \cite{Vorderer2003} call Social Competition is a staple in the design of many Core Games in many different forms, e.g. leader boards \cite{Kaye2012} or PvP battles \cite{Elliott2012}. Due to the relevance of this design element in Core Games, I assume that Core Games can generally increase the significance of the the "competition" motive for the use of video games if competitive elements are included.
\item Achievement - Achieving victory, defeating the game is the main goal of many, at least single-player, Core Games.\cite{Adams2000} In Core Games it is about competeting with the game, its obstacles and non-player characters and overcoming those barriers. \cite{Vorderer2003} I propose that Core Games intensify the "achievement" motive for the use of video games. But as Casual Games might have the some intensification effect there will be no obvious correlation.
\item Escapism - \cite{Yee2007} identified escapism, using the gaming environment to avoid thinking about real life problems, as the best predictor for time spent using video games, in his research the MMORPG World of Warcraft. As Core Games are designed to be used for longer periods of play time and often feature distinct characters the player identifies with or even creates as an idealized version of himself \cite{Hefner2007}, they are perfectly suited to provide the means to escape from real life. I assume that Core Games propagate the "escapism" motive for the use of video games, but without being able to be distinguished from Casual Games, that employ the same effect.
\item Altering Emotional States - \cite{Zillmann1988}'s mood management theory is used to explain the motivation for the use of video games in literature \cite{Rieger2014,Reinecke2009,Williams2008,Sherry2006} in order to explain, why games might be selected and used. \cite{Oliver2016} add another element to gaming, as they illustrate how even action-oriented Core Games like the Grand Theft Auto series, add depth to their narrative to provide meaningful experiences to players. This is one way Core Games could support the motive of "altering emotional states". But this cannot be attributed to Core Games only, therefore I conclude that I do not expect Core Games to have an amplifying effect on the "altering emotional states" motive.
\item Time Killing - Due to their high ressource demands \cite{Juul2010}, including the requirement for longer time slots for game sessions, I propose that Core Games do not facilitate the "Time Killing" motive. They may be used to counter boredom, but I assume that their main purpose will be another.
\item Immersion - Core Games are highly immersive and engaging games as \cite{Kultima2009} describes them in opposition to Casual Games. Immersion can be achieved with the use of high-fidelity graphics and sound to create realistic 3D game worlds. \cite{Weibel2011,Gallagher2002} These elements are tied to Core Games, therefore I deduce that the "immersion" motive for video games use is amplified by Core Games.
\item Self Efficacy and Effectance -  As all digital games are based on input-output-loops that provide immediate feedback to every player action, they support the effectance motivation as explained by \cite{Klimmt2006}. Even though there are differences in how that interactivity takes place, Core Games tend to feature diegetic juiciness as \cite{Juul2010} calls it, that means, excessive positive feedback to player actions inside the game world. Casual Games make use of non-diegetic, that is player directed feedback. As both kinds of game support effectance based motives, I conlude, that there will be no difference between the game types in their effect on the "self efficacy and effectance" motive for the use of video games.
\item Exploration - Understanding and learning is at the core of most Core Games, e.g. exploring new areas in RPGs, learning new tactics in MOBAs or sports games or finding the best route in a platform game level. This is based on the higher complexity of Core Games, that foster this behavior. Therefore I propose that Core Games will have an amplifying effect on the "exploration" motive for the use of video games.
\end{itemize}

\subsubsection{Hypotheses regarding the effects of Casual Games on Motives for Video Game Use}

\begin{itemize}
\item Sociability - In multiplayer games there are three considerations a player has to balance: goal orientation, game experience and social management. Casual Games offer the chance to focus on any combination of them, as well as Core Games do. \cite{Juul2010} As there is no foundation for Casual Games to amplify the "sociability" motive more than Core Games there is no effect of them on the motive to be predicted.
\item Competition - Competition can be included as an element of Casual Games, like leader boards or tournaments, usually Casual Games focus on the competition with oneself. \cite{InternationalGameDevelopersAssociation2009} This "internal" competition is not included in the "competition" motive as defined in this paper, which is based on the social competition as described in \cite{Vorderer2003}. It is rather included in the achievement motive. Following this argumentation I reason that Casual Games do not increase the effect of the "competition" motive on the use of video games.
\item Achievement - As mentioned regarding the motive of "competition", Casual Games focus on an introvertive competition which is another way to descripe the strive for accomplishement or achievement. \cite{InternationalGameDevelopersAssociation2009} This is reconfirmed by \cite{Juul2010} who states that Casual Games are desired to be easy, but easy as in easy to use, but should provide ample difficulty. I conclude that Casual Games have an enforcing effect on the "achievement" motive because they encourage continued video games use by balancing difficulty and player skill to create the constant feeling of accomplishment in the player. As Core Games have a similar effect, I suspect no visible effect of the games type on this motive.
\item Escapism - \cite{InternationalGameDevelopersAssociation2009} states as a design principle that Casual Games should provide escapism through theme and artistic direction, because Casual Gamers are supposed to view games as an escape from their daily life. It is mentioned that Casual Games should therefore differ from real world life and actions. If that is the case then Casual Games could have an intensifying effect on the "escapism" motive. Nevertheless I do not expect that Casual Games will have a significantly different effect on the motive than Core Games.
\item Altering Emotional States - \cite{Zillmann1988}'s mood management theory is used to explain the motivation for the use of video games in literature. \cite{Rieger2014,Reinecke2009,Williams2008,Sherry2006} Casual Games are described as relaxing rather than exhilarating and creating feelings, that the gamers want to experience again. \cite{InternationalGameDevelopersAssociation2009} I assume that Casual Games might have a minor amplifying effect on the "Altering Emotional States" motive for the use of video games, that will not be obvious due to its counterpart, Core Game, having a similar one.
\item Time Killing - Due to their on flexibility and interruptibility focussed game design, Casual Games will have an intensifying effect on the "time killing" motive, because they can be played for very short time intervals as well as very long one, making them fit to a lot of occasions where boredom can arise.
\item Immersion - One of the game design principles for Casual Games is to prefer "style over realism", because the escapism provided by Casual Games is more significant than immersion to their target audience. \cite{InternationalGameDevelopersAssociation2009} Casual Games can make the player experience flow, which is considered to be a pre-condition for feeling immersed in a game world. But additionally it is required to feel present in the game world. \cite{Weibel2011}  This is not supposed to be achieved by most Casual Games and I consequently state, that Casual Games may not have an amplifying effect on the "immersion" motive. This hypothesis is supported by \cite{Kultima2009} who lists low immersion as one characteristic  that Casual Games are touted for.
\item Self Efficacy and Effectance - As already explained in regards to the effect of Core Games regarding the "self efficacy and effectance" motive for the use of video games, I propose, that there will be no difference induced by the game type on this motive, because both types massively support it, only differing in the used techniques.
\item Exploration - Casual Games frequently reward discovery and use exploration as game theme \cite{InternationalGameDevelopersAssociation2009}, so one can assume that the "exploration" motive is supported by Casual Games. As Core Games might focus more on this than Casual Games, I propose, that Casual Games will not have a significant effect on the "exploration" motive.
\end{itemize}

\subsubsection{Hypotheses regarding the effects of Core Games on Barriers against Video Game Use}

\begin{itemize}
\item Aesthetics and Moral Reasons- The aesthetics of Core Games are designed with their target audience in mind and often feature topics that may create a negative valence, like science-fiction, horror, war, destruction and fighting themes. \cite{Juul2010, Lane1999, InternationalGameDevelopersAssociation2009} Therefore I argue, that Core Games may increase the barriers "aesthetics" and "moral reasons" depending on their specific setting and design, e.g. a game like "Gears of War" may hinder video games use for distinct individuals more than the setting and design of "Pro Evolution Soccer", which can be equally regarded as a Core Game.
\item Inaccessibility of Game Devices - Core Games are designed with the goal of providing immersion and the suspension of disbelief \cite{Kuo2016} by the use of high-fidelity graphics and sound. \cite{Weibel2011,Gallagher2002} This makes them require powerful hardware, like current generation gaming consoles or gaming PCs, which are not that widely available as smarthones or office PCs. \cite{Juul2010} I propose that Core Games raise the barrier of the "inaccessibility of game devices" due to their higher hardware requirements.
\item Inaccessibility of Games - As digital platforms like Steam with more than 17,000 available games \cite{SteamSpy2017} are becoming the dominant distribution channel for video games, I argue, that Core Games do not increase the barrier "inaccessibility of games".
\item Costs - In comparison to their casual counterpart, Core Games are mainly sold on stationary gaming consoles and PCs. The average price in Germany for a PC game  was estimated by \cite{Statista2016} to be at 17.40€, for a console game the average price was at 36.71€ \cite{Statista2013}. \cite{Pocketgamer2017} reports an average price for a mobile game of 0.49US\$. From these numbers I infer the hypothesis that Core Games might increase the significance of the "costs" barrier against the use of video games.
\item Time Constraints - According to \cite{Adams2000} Core Games are designed to occupy their users for longer time periods. This is in line with the findings of \cite{Juul2010} that Core Games regularly do not offer the same amount of interruptibility that Casual Games do. I propose that Core Game increase the significance of "time constraints" as a barrier against video game use.
\item Complexity - Core Games usually require a certain knowledge of gaming conventions \cite{Gandasegui2010, Juul2009} and due to the preferred difficulty of their target audience \cite{Adams2000}, they place a significant cognitive load on their users. The consequence from these requirements is an increased barrier effect due to their "complexity" against the use of video games.
\item Unfamiliarity - The subject of "unfamiliarity" is a direct continuation of the complexity already mentioned. Individuals that are not aware of gaming conventions \cite{Gandasegui2010, Juul2009}, that are not familiar with gaming and are unable to "master the controls" \cite{Przybylski2010} will be significantly hindered regarding the use of video games, making unfamiliarity a barrier that is enforced by Core Games.
\item Too much choice/Hyperchoice - \cite{SteamSpy2017} currently counts more than 17,000 games on Steam, which is indicating that hyperchoice can be a real issue regarding core games. I propose that Core Games due to their massive availability increase the significance of hyperchoice as a barrier against the use of video games.
\end{itemize}

\subsubsection{Hypotheses regarding the effects of Casual Games on Barriers against Video Game Use}

\begin{itemize}
\item Aesthetics and Moral Reasons - Casual Games are designed with a high acceptability in mind. This includes creating a positive valence \cite{Juul2010, Lane1999} by using positive fiction in combination with safe and familiar topics \cite{Kultima2009} and focussing on making a game suitable for larger groups by employing design principles like \cite{Kultima2009} mentions: match the players' social context, avoid offensive topics, use themes that have a more universal appeal or that focus on positive mechanics like building or creating instead of killing or fighting. Application of these principles creates games that presumably more people at least do not find unaesthetic or violating their personal code of conduct. Consequentally I argue, that Casual Games lower the "aesthetics"- and the "moral reasons" barriers.
\item Inaccessibility of Game Devices - Even though a game itself cannot influence the accessibility of game device, Casual Games are designed with low hardware requirements, because Casual Gamers usually do not spend money on specialized game hardware or peripherals. \cite{InternationalGameDevelopersAssociation2009} Therefore Casual Games assume just the minimal standard hardware for a given platform. \cite{InternationalGameDevelopersAssociation2009} I formulate the hypothesis, that Casual Games lower the "inaccessibility of game devices" barrier by running on low-end, commodity devices that are widespread in their target audience.
 \item Inaccessibility of Games - Casual Games are created with the principle of accessibility in mind. These games are easily available via digital distribution channels \cite{IGDA2006} in order to tap into a maximal user base. Therefore I propose that Casual Games, by improving accessibility, lower the "inaccessbility of games" barrier significantly.
\item Costs - Casual Games are generally placed in a lower price segment when compared to Core Games. As \cite{Kultima2009} states, a lot of them are designed "so that at least some parts of the experience are accessible with low investments." This can be achieved by using the "freemium"-model \cite{Anand2014} in combination with microtransactions. \cite{InternationalGameDevelopersAssociation2009,Kultima2009} With the employment of low price points or the freemium-approach Casual Games effectively lower the "costs"-barrier.
\item Time Constraints - Casual Games design principles include the call for a minimal time investment required from the player, creating the feeling, that the game can be left any time. \cite{InternationalGameDevelopersAssociation2009} This characteristic is called "interruptibility" \cite{Juul2010}, which means that Casual Games allow the player to play only for short periods of time without limiting the session length. Another aspect regarding time constraints is the possibility to use Casual Games as a secondary medium that can be used parallel to another medium, e.g. playing a game while watchting TV. \cite{Kultima2009} This makes it possible to effectively double the time spent for media use. Interruptibility and the opportunity to use Casual Games as a secondary medium make them lower the barrier "time constraints".
\item Complexity - Reducing complexity in order to increase the accessibility of Casual Games is one of their main design goals. This includes making them easier on the cognitive load, for example by using topics that are familiar from other contexts \cite{Trefry2010} or imitating mechanics from real world games. \cite{Kultima2009} By providing ample information in easily digestable amounts in the adoption phase the need to know the conventions of gaming is significantly lowered. \cite{Juul2010} Casual Games continue on their mission to reduce complexity by utilizing simplified control schemes, that are appropriate to their platform. \cite{InternationalGameDevelopersAssociation2009} It is easily discernible that one can argue, that Casual Games lower the "complexity" barrier.
\item Unfamiliarity - To continue my argumentation from the complexity topic, Casual Games try to reduce barriers against gaming for players unfamiliar with games, by letting them use control mechanisms they are used to, like a mouse for PC games or touchscreen interfaces on smartphones or handheld devices and in this way reducing the cognitive load to learn how to use those input devices correctly. \cite{InternationalGameDevelopersAssociation2009} Techniques used to reduce complexity, like using familiar topics or mechanisms \cite{Kultima2009, Trefry2010}, makes it easier for unexperienced individuals to use Casual Games.
I establish the hypothesis that Casual Games lower the barrier of "unfamiliarity" by reusing familiar mechanisms and reducing complexity, requiring little to no knowledge about gaming before starting to play and by rewarding every player action in a very juicy, non-diegetic way to create the feeling of being positively rewarded. \cite{Juul2010}
\item Too much choice/Hyperchoice - Regarding the issue of hyperchoice there is no countermeasure to be observed when it comes to Casual Games. The Apple App Store, probably one of the most important distribution channels for Casual Games, features at the moment of writing 785,060 games with 86 new games being released per day. \cite{Pocketgamer2017} I follow this with the argumentation that Casual Games in their current form and over-availability increase the issue of hyperchoice.
\end{itemize}

\cleardoublepage
\bibliographystyle{apalike-url}
\renewcommand\bibname{References}
% \nocite{*}
\bibliography{library}
\clearpage
\end{document}
