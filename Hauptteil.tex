\cleardoublepage
\pagenumbering{arabic} \setcounter{page}{1}

\section{Introduction (˜1p)}\label{introduction-1p}

``Video games are the future. From education and business, to art and
entertainment, our industry brings together the most innovative and
creative minds to create the most engaging, immersive and breathtaking
experiences we´ve ever seen.'' - \emph{Michael D. Gallager (President
and CEO of the Entertainment Software Association)}

The video games market is regarded as being the fastest growing segment
of the entertainment branch \cite{Hennig-Thurau2013} and current numbers
heavily back this assessment: The yearly revenue for the global games
market is estimated to be around 99 billion US\$ in
2016\cite{NewZoo2016}, while digital games alone made over 35 billion
Euros in last year. The CAGRS (compound annual growth rate) is being
estimated to be around 7\%, so breaking the 50 billion Euro barrier in
2020\cite{Statista2016a}. Mobile games alone are generating nearly 37
billion US\$ in 2016 and are on their way to overtake the film industry
as \cite{PWC2015} reports a yearly global box office revenue for 2015
sneaking up to 40 billion Euro and growing at just 5.7\%. The gamer
community consists of more than 2.1 billion people worldwide nowadays.
\cite{NewZoo2016}

Entertainment company Sony has sold more than 40 million Playstation4
video game consoles since launching it in November 2013 \cite{Sony2016},
including the most successful gaming console launch of all times with
selling more than one million devices in the first 24 hour
\cite{ShuheiYoshida2013}.

!TODO Insert numbers of most successful games/launches here (GTA V,
FIFA17)

And the old stereotype of the teenage male nerd gamer is not applicable
any more with the average US male gamer being 35 years of age and his
female counterpart clocking in at 44 years.\cite{ESA2016}

This massive success has turned the video games industry into playing a
pivotal role in everyone´s daily life \cite{ESA2016a} and inspired
researchers all around the world to dig into it. \cite{Sun2016}
investigate the effects of consumer heterogeneity on the video games
market, \cite{Binken2009} look into the role of high quality software
for hardware sales, while \cite{Shankar2002} and \cite{Clements2005}
concentrate on indirect network effects(e.g.~hardware-software
interdependencies). On the other hand \cite{Aoyama2003} approach gaming
from a socio-cultural perspective. In their article
\cite{Hennig-Thurau2013} introduce a conceptual framework for value
creation in the video game industry and do place the gaming platform in
the center of gravity, but there is no major focus on the determination
of success factors for the gaming platforms themselves.

This seminar paper aims to fill this gap and provide a comprehensive,
conceptual framework to answer the question: ``Which ingredients turn a
video game console into a successful platform for a specific target
group?'' To create this framework I will first lay out the foundations
by introducing the gaming industry´s main characteristics, e.g.~that it
is a two-sided, hedonic market catering to different gamer personae as
its target audience and experiences a regular soft reset every 5 to 7
years with the introduction of a new console
generation.\cite{Gallagher2002}

I will then use the framework provided by \cite{Hennig-Thurau2013} as a
starting point and expand it to establish a holistic picture of a video
game platform (see figure \ref{fig:framework} using the current state of
scientific research. To conclude this paper I will apply the framework
onto the the ``console war'' of the seventh console generation and
discuss if the Apple iPhone has to be identified as a successful gaming
platform.

\section{Foundations (˜1.5p)}\label{foundations-1.5p}

Researching success factors is a common scientific
practice\cite{Leidecker1984} in other branches like the film industry
\cite{Hennig-Thurau2001}, supply chain management \cite{Power2001} or
software development \cite{Reel1999}, but not as common in video games
research thus opening interesting research opportunities.

As a precondition to investigating the success factors of video game
platforms we have to establish the characteristics of the video games
industry. Video games and thus video game platforms are by definition
hedonic products like movies !TODO (reference here!) as they deliver a
multisensoric experience on an emotional level. The relevant criterion
in the buyer´s decision process is to evaluate if the video game
platform delivers a desirable experience. \cite{Hirschman1982}

There is an inherent need to differentiate the potential buyers of video
game consoles into targettable segments according to their individual
and shared desired experiences. \cite{Kuittinen2007} and \cite{IGDA2006}
identify casual gamers, core gamers and hardcore gamers, without those
segments being mutually exclusive. The hardcore gamer is a highly
competetive personality that prefers games requiring a high degree of
skill (e.g.~hand-eye-coordination, low reaction times, tactics) and a
steep learning curve. The casual gamer on the opposite side of the
spectrum prefers low-involvement games, that can be played with a lower
time investment, which she does not necessarily do. There happen to be a
lot of casual gamers playing ``casual games'' with a ``hardcore'' time
investment. \cite{Kuittinen2007} The core gamer hovers between those
extremes. \cite{Scharkow2015} But all gamer personae have a common
denominator what they expect from their gaming platform of choice: an
enjoyable experience. (see figure \ref{fig:personae})

\begin{figure}[ht]
\centering
    \begin{subfloat}
        \includegraphics[scale=0.2]{Images/gamer_personae_1}
    \end{subfloat}
    \begin{subfloat}
        \includegraphics[scale=0.2]{Images/gamer_personae_2}
    \end{subfloat}
    \caption{Matrix of different gamer personae / Source: Own design based on \cite{Kuittinen2007}, \cite{IGDA2006}, \cite{Juul2009}}
    \label{fig:personae}
\end{figure}

\cite{KatzMichaelL.1994} describe in detail the indirect network effects
that exist in system markets like the video games industry. A bigger
installed base, e.g.~more consoles sold to gamers, leads to a greater
variety in software, e.g.~more games available for said gamers, a higher
software qualitity, better games, and thus increasing the value of the
gaming platform for the consumer. Additional direct network effects are
the foundation of \cite{Marchand2016} where the approach is coming from
the opposite direction, e.g.~how to use the software to counter
lifecycle decline. So the software-hardware interdepency is a given fact
in the video games industry.\cite{Clements2005}

Another key feature defining the video game console market is the
existence of the ``console generations'', that effectively lead to a
soft reset when the main players introduce their new platforms in a
timeframe of about 12 to 24 months. This ``soft reset'' is fueled by
technological innovation \cite{Orland2013}(following ``Moore's law''
\cite{Moore1965}). Until this day there have been eight console
generations (see figure \ref{tab:generations})entering the market and
being part of the ``console wars'' in which they battle for market
domination during their life cycle. \cite{EncyclopediaGamia2016} As the
definition of a console generation is nothing that adheres to scientific
criteria I accept and use the common timeframes like they are stated on
\cite{Wikipedia2016} and shown in figure \ref{tab:generations}.

\begin{figure}[h]
    \begin{tabular}{  l  l  l }
    \hline
        Generation  &   Timespan    &   Main Competitors    \\ \hline
        1&  1972-1980   &   Magnavox Odyssey, Coleco Vision \\ 
        2&  1976-1992   &   Atari 2600, Mattel Intellivision    \\ 
        3&  1983-2003   &   Nintendo Famicom/NES, Sega Master System, Atari 7800    \\ 
        4&  1987-2004   &   Super Famicom/SNES, Sega Genesis, PC Engine, Neo Geo    \\ 
        5&  1993-2005   &   N64, Sony PlayStaion, Sega Saturn, Atari Jaguar \\ 
        6&  1998-2013   &   Dreamcast, PlayStation 2, Gamecube, Microsoft XBox  \\ 
        7&  2005-today  &   Wii, PlayStation 3, XBox 360    \\ 
        8&  2012-today  &   Wii U, PlayStation 4, XBox One  \\ \hline
    \end{tabular}
    \caption{The eight console generations and their main competitors / Source: Own design based on \cite{Wikipedia2016}}
    \label{tab:generations}
\end{figure}

\cite{Hennig-Thurau2013} propose a framework of value creation in the
video game industry in which the gaming platform is the center of
gravity and part of the vertical path of the so called ``gaming
environment''. I will now zoom in to develop a conceptual framework for
the determination of the needed ingredients to turn a video game console
into a successful gaming platform.

\section{Developing a conceptual
framework}\label{developing-a-conceptual-framework}

\subsection{Overview}\label{overview}

Piling on the foundational characteristics of the video games industry
and the framework of \cite{Hennig-Thurau2013} I constitute the basic
building blocks for a conceptual framework to determine success factors
for video game consoles: The hardware-software interdependency according
\cite{KatzMichaelL.1994} and \cite{Clements2005} provides the first two
entities: hardware and software, heavily depending and influencing on
each other.

This paper introduces a third column, value added digital services. As
\cite{CapGemini2015} state, the digital transformation of the business
world is ``a fact of life and a sweeping force for business change.''
Ignorance bears the potential of being victim to the ``innovator´s
dilemma''. \cite{christensen2003innovator} The positive influence of
value added services in other markets like cell networks\cite{Kuo2009},
messaging apps as \cite{Line} and digital music distribution
\cite{Bockstedt2006} has been demonstrated as well as scientifically
explored and leads to the conclusion that they will also play a vital
role in the success of gaming platforms. These digital services depend
on the console hardware as a basic layer and on the available software
which they expand upon.

The value proposition of the video game platform as a hedonic product is
to provide the gamer with a highly desirable joyful experience.
Enjoyment in the gamer´s perspective stems from the ability of the
gaming system (hardware, software and value added services) to provide
an immersive flow experience. The concept of immersion is still not
fully explored, but can be understood as a lack of awareness of time and
space of the real world and being higly involved in the gaming
environment. \cite{Jennett2008} Immersion is also a key component in
enabling the player to experience the feeling of ``flow''.
\cite{Sweetser2005}

The three entities hardware, software and value added digital services
are the ingredients of the video games platform that has to be
positioned by the producer in way that the brand image and its value
proposition, the enjoyable experience, are highly congruent with the
subjective expectations of the targeted gamer/consumer segment in order
to be highly relevant for the consumers´ behavior. The process of
positioning will not be discussed in detail in this paper as it is out
of its scope and sufficiently covered in literature. \cite{Aaker1982}
\cite{Feddersen2010}

The combination of the before mentionend items including the positioning
process and the desired experience leads to the conceptual framework as
shown in figure \ref{fig:framework}. The ingredients will be
individually explored in detail in the following chapters.

\begin{figure}[ht!]
    \includegraphics[scale=0.65]{Images/Framework_Entwicklung_3}
    \caption{Draft of a comprehensive framework for the determination of success factors for video game platforms / Source: Own design}
    \label{fig:framework}
\end{figure}

\subsection{Hardware}\label{hardware}

As immersion is one of the main components of games enjoyment this
constitutes the need to identify a way to create the feeling of
immersion for the gamer. \cite{Steuer1992} uses the term
``telepresence'' to describe the experience of the player of being
timely and spatially present in a mediated environment, e.g.~the ``game
space'', and losing awareness of the real environment, e.g.~the ``player
space''. Additionally to get into a state of flow during the gaming
experience the player must be able to feel in full control of the action
on the screen. \cite{Sweetser2005} So the video game platform has to
equip her with an effective and efficient control interface to enable a
high degree of interactivity.\cite{Skalski2011}

\subsubsection{Immersion powered by high-performance hardware
(˜1p)}\label{immersion-powered-by-high-performance-hardware-1p}

One way to support feeling of being present in a simulated environment,
e.g.~the game world, is by the use of realistic 3D gamescapes powered by
modern hardware. \cite{Kuo2016} In the first six console generations
improving graphics and sound to facilitate a more immersive gameplay by
providing a way of experiencing spatial presence \cite{Weibel2011} has
been a main driver of technological innovations in gaming platforms and
also enabled new challengers to enter the market in a console generation
successfully as Sony demonstrated with the first
Playstation.\cite{Gallagher2002}

The player´s expectations are strongly interlinked with her gamer
persona and so defining the amount of performance needed. Core and
hardcore gamers expecting highly involving and complex games as shown
above are the right target segment for high fidelity graphics and
bombastic sound. Casual gamers require gaming experiences that are
highly accessible. Accessibility can be achieved by using less complex
graphics leading to an easier visual acquisition of relevant game
information. Immersion and accessibility through simplicity are not
mutually exclusiv as the game Tetris demonstrates. The clear design is
easy to understand and does not require a powerful platform.
Nevertheless, a lot of Tetris players are ``sucked into'' the game, lose
their awareness of time and space in the real word and are immersed in
the game space. \cite{Jennett2008} This leads to the conclusion that
high graphic and sound standards can play an important role in providing
immersive game experiences on video game platforms, but they are not
required as shown in figure \ref{fig:screenshots} which compares the
graphics quality of Tetris with the major success title GTA V that was
developed with the core gamer in mind. I assess that hardware
capabilities can only offer the foundation for such experiences but
without the matching games this offer can be in vain. It is required to
correlate the targeted gamer segment with the hardware used to construct
the gaming platform.

\begin{figure}[ht!]
 \centering
    \begin{subfloat}
        \includegraphics[scale=1.0]{Images/GB_Tetris}
    \end{subfloat}
    \begin{subfloat}
        \includegraphics[scale=0.45]{Images/gtav}
    \end{subfloat}
    \caption{Screenshots of Tetris and GTA V/ Source: Nintendo/Rockstar Games}
    \label{fig:screenshots}
 \end{figure}

Targetting the casual gamer segment with gaming platforms not conforming
to the traditional ``race of arms'' is a strategy that evolved in the
last two generations, mainly driven by the Nintendo Wii. \cite{Juul2009}
Having a look how the amount of system memory, one of the main factors
defining graphical complexity, increased over the last console
generations one easily identifies Nintendo breaking away in Generation
seven, after more than two decades of being on par with the competition.
(See Figure \ref{fig:consoleram}) This development correlates with
shifting the focus away from the core gamer segment towards the casual
segment as described in \cite{Juul2009}.

\begin{figure}[h]
\centering
\begin{tikzpicture} 
       \begin{semilogyaxis} 
          [
             width=\linewidth*.65, % Scale the plot to \linewidth
              grid=major, 
              grid style={dashed,gray!30},
              xlabel=console generation, % Set the labels
              ylabel=RAM in bytes,
              legend pos = north west,
              x tick label style = {rotate = 0, anchor=north, /pgf/number format/1000 sep= },
              y tick label style = {/pgf/number format/1000 sep= },
              xtick distance = 1,
              minor x tick num = 1,
              ymin = 0, xmin = 2.5,
              ymax = , xmax = 8.5,
              legend entries = {$Sony$, $Microsoft$, $Nintendo$,$Sega$}
          ]
          \addplot table [col sep=comma, x = Generation, y = S_RAM]{ConsoleRAM.csv};
          \addplot table [col sep=comma, x = Generation, y = MS_RAM]{ConsoleRAM.csv};
          \addplot table [col sep=comma, x = Generation, y = N_RAM]{ConsoleRAM.csv};
           \addplot table [col sep=comma, x = Generation, y = SEG_RAM]{ConsoleRAM.csv};
       \end{semilogyaxis} 
    \end{tikzpicture}
    \caption{System memory of video game platforms / Source: Own design based on \cite{Wikipedia2016}}
    \label{fig:consoleram}
\end{figure}

\subsubsection{Game Flow enabling control interface
design(˜1p)}\label{game-flow-enabling-control-interface-design1p}

The control interface, usually a gamepad or joystick, is the defining
element of the interaction between the gamer and the game itself.
\cite{Kelechava2015} Sony rated the importance of this interface so
high, so that during the PlayStation 4 launch event they did not bother
to show the console itself, but just the gamepad. \cite{Stuart2016}

Over time the control interfaces evolved from the simple digital, one
button joystick of the Atari 2600 into complex devices like the Xbox 360
Gamepad with its 11 buttons and 6 analogously controlled axis of
maneuver or the Wii U Gamepad that incorporates a touch display on top.
(See figure \ref{fig:buttons} in Appendix \ref{app:figures} and figure
\ref{fig:controllermatrix} below) \cite{Brunner2013}

\begin{figure}[ht!]
\centering
    \includegraphics[scale=0.35]{Images/controllermatrix}
    \caption{Rising complexity of video game control interfaces / Source: Own design}
    \label{fig:controllermatrix}
\end{figure}

In order to gain the ability to target a specific gamer segment with the
according controller design there has to be a way of correlating
properties of controllers to the desired experience of the targeted
gamer persona. To solve this issue I propose the classification of
control devices according figure \ref{fig:controllerclassification}. The
table in figure \ref{tab:controllerclassification} applies the
classification on some exemplary controllers and identifies the focussed
gamer personae. To understand the terms used please refer to figure
\ref{tab:controllerclassificationterms} where they are defined and
referenced.

For ease of understanding imagine playing a golf simulation with either
a) classic gamepad or b) the Wiimote. The classic gamepad is
artificially mapped and imitating the actions on screen via button
presses, e.g.~press button ``A'' to start the swing, press it again to
determine the amount of power, press a third time to finally swing the
bat and use one of the analog sticks to control the golf ball´s spin. In
contrast the Wiimote offers an imitation approach, as the players action
resemble the on screen action as closely as possible. It is also a
showcase for natural mapping because the the use of the controller in
this case correlates with a high degree to the real life movements
performed while playing golf. Natural mapping has been show to increase
the feeling of spatial presence in the game environment, thus
facilitating immersion effects, and leading to a high enjoyment factor.
\cite{McGloin2011}

While \cite{Kelechava2015} rates controller design evolution as not
being significantly improved from one generation to the next, I consider
it as a vital component of a modern video game platform design,
especially in the process of positioning the platform for a specific
target audience by offering the right mixture of
complexity/accesibility, type of control and mapping to increase gamer
enjoyment and immersion.

\begin{figure}[ht!]
\centering
    \includegraphics[scale=0.35]{Images/controllerclassification}
    \caption{Classification matrix for control devices / Source: Own design based on \cite{Jenson2008}, \cite{Skalski2011} and \cite{McGloin2011}}
    \label{fig:controllerclassification}
\end{figure}\begin{figure}[h]
    \centering
    \small
    \begin{tabular}{  l  l  l  l  l  l }
        Controller      &   Type of Control &   Mapping         &   Complexity  &   Precision   & Focus\\ \hline
        NES Gamepad     &   Imitation       &   artifical       &   low         &   medium      & Core\\ 
        XBox 360 Gamepad&   Imitation       &   artificial      &   high        &   high        & Core/Hardcore\\ 
        Wii U Gamepad   &   Imitation       &   artificial      &   high        &   medium      & Core\\ 
        Wiimote         &   Simulation      &   artifical/natural&  medium      &   low/medium  & Casual\\ 
        DanceMat        &   Simulation      &   natural         &   low         &   high        & Casual\\ 
        FlightStick     &   Simulation      &   natural         &   high        &   high        & Hardcore\\ 
        GuitarHero Controller&  Simulation  &   natural         &   low         &   medium/high & Casual\\ \hline
    \end{tabular}
    \caption{Classification of exemplary controllers / Source: Own design}
    \label{tab:controllerclassification}
\end{figure}\begin{figure}[h]
    \centering
    \small
    \begin{tabular}{  l  p{5cm}  p{4cm}  }
        Term        &   Definition  &   Reference   \\ \hline
        Mapping     &   How are the performed actions of the gamer connected to corresponding changes in the game space &   \cite{Steuer1992}\\
                    &   &   \\
        Natural Mapping & Matching the action performed by the gamer and the natural action as closely as possible & \cite{Skalski2011}, \cite{McGloin2011}, \cite{Steuer1992}\\
            &   &   \\
        Artificical Mapping &   Arbitrary and completely unrelated mapping of gamer action to the function performed & \cite{Skalski2011}, \cite{McGloin2011}, \cite{Steuer1992}\\
            &   &   \\
        Simulation  &   Translation of  the player´s input into a (character) action on screen, e.g. the player presses the "A" button and the character jumps& \cite{Jenson2008}\\
            &   &   \\
        Imitation   &   The player´s imitative action corresponds to the action on screen, e.g. the player swings the controller and the golf bat on screen swings as well& \cite{Jenson2008}\\
            &   &   \\ \hline
    \end{tabular}
    \caption{Control device classification terms in literature / Source: Own design}
    \label{tab:controllerclassificationterms}
\end{figure}

\subsection{Software}\label{software}

\subsubsection{Indirect network effects - Variety is the spice of
life(˜1.5p)}\label{indirect-network-effects---variety-is-the-spice-of-life1.5p}

indirect network effects\cite{Cenamor2013} and \cite{Clements2005} and
\cite{Shankar2002}

\subsubsection{Looking for the system sellers - The need for high
quality software
(˜1.5p)}\label{looking-for-the-system-sellers---the-need-for-high-quality-software-1.5p}

Besides indirect network effects as discussed above, a video game
platform does not only need a certain amount of variety in the available
software, but also the perceived quality plays an important role in its
success. \cite{Binken2009} showcase that ``superstar games'' are system
sellers, as 1 in 5 buyers (in the six month period after the game
launched) of a superstar game also purchases the hardware necessary to
play that game, making it effectively a system seller.

!TODO

\begin{itemize}
\tightlist
\item
  identify superstar games in the current, 8th, generations
\item
  check out launch lineups and the quality
\item
  check for correlation of superstar software genre and target audience?
  (HALO Xbox, Mario Wii, Uncharted PS)
\end{itemize}

\begin{figure}[h]
    \centering
    \small
    \begin{tabular}{  l  r  r r  }
                        & Playstation 4 & Xbox One  & Wii U \\ \hline
            Titles      & 25            & 22        & 26    \\
            Median      & 75.96         & 74.59     & 71.21 \\
            Mean        & 74.24         & 68.76     & 63.36 \\
    \textgreater 85     & 5             & 2         & 3     \\
    \textgreater 80     & 5             & 2         & 6     \\
    \textless 65        & 3             & 4         & 4     \\
    \textless 50        & 2             & 2         & 7     \\ \hline
    \end{tabular}
    \caption{Launch lineup quality of the 8th console generation/ Source: Own design}
    \label{tab:gen8quality}
\end{figure}

\subsection{Value added digital services
(˜2p)}\label{value-added-digital-services-2p}

!TODO - research regarding digital services in gaming?

\begin{itemize}
\tightlist
\item
  Multiplayer Gaming

  \begin{itemize}
  \tightlist
  \item
    social gaming, competetive gaming\cite{Domahidi2014}\cite{Hsu2004}
  \item
    direct network effects\cite{Marchand2016}
  \end{itemize}
\item
  Social factors

  \begin{itemize}
  \tightlist
  \item
    social experiences
  \item
    flow in social gaming
  \item
    social interaction via chat, voice \cite{Trepte2012}
  \item
    self-expression (achievments, gamer score, \ldots{})
  \end{itemize}
\item
  Digital game distribution

  \begin{itemize}
  \tightlist
  \item
    use for consumer: ease of consumption
  \item
    use for producer: direct sales, no logistics, vendor lock in -
    switching costs
  \end{itemize}
\item
  Technical requirements

  \begin{itemize}
  \tightlist
  \item
    stability
  \item
    responsiveness
  \item
    ease of use, User Interface Design
  \end{itemize}
\end{itemize}

\section{Application of the
framework}\label{application-of-the-framework}

\subsection{The console war in generation 7
(˜2p)}\label{the-console-war-in-generation-7-2p}

The war waging parties

\begin{itemize}
\tightlist
\item
  Nintendo Wii
\item
  Sony Playstation 3
\item
  Microsoft Xbox 360
\end{itemize}

!TODO

\begin{itemize}
\tightlist
\item
  Positioning of the consoles, mainly via advertising, see if articles
  available
\item
  hardware specs
\item
  controller design - classification according proposed criteria
\item
  value added services - overview, target audience, issues?
\item
  sum up in table form
\item
  ``declare'' a winner
\end{itemize}

Nintendo Wii

\begin{itemize}
\tightlist
\item
  targetting/positioned for casual gamers and former non-gamers
\item
  controller design, motion control
\item
  reduced
\end{itemize}

Advertisement slogans:

\begin{itemize}
\tightlist
\item
  Wii move you
\item
  Wii would like to play (campaing with families, all ages, casual
  focus!)
\end{itemize}

Sony Playstation 3

Advertisement slogans:

\begin{itemize}
\tightlist
\item
  It only does everything
\item
  Long live play
\item
  The game is just the start
\item
  This is living
\item
  Dance with Zombies before blowing their heads off
\item
  Go beyond the game
\end{itemize}

Microsoft Xbox 360

Advertisement slogans:

\begin{itemize}
\tightlist
\item
  Jump in
\item
  The next generation now
\item
  Kinecting people
\end{itemize}

\subsection{The Apple iPhone - a video games platform?
(˜1.5p)}\label{the-apple-iphone---a-video-games-platform-1.5p}

Notes:

\begin{itemize}
\item
  is it a gaming platform? come up with numbers, e.g.~AppAnnie,
  performance GeekBench \cite{AppAnnie2016}
\item
  apply framework and show why it´s a successful gaming system
\item
  simple controller design = direct input via touch
\item
  huge game quantity = variety
\item
  lot of high quality games = show via metacritic
\item
  discuss if there are any superstar software title
\item
  discuss Foster´s S Curve model and application to the
  iPhone/Smartphone as a gaming device
\item
  Positioning
\item
  Hardware
\item
  Software
\item
  Value added digital services
\item
  It is an innovative, disrupting gaming platform
\end{itemize}

\section{Discussion, limitations and future research
(˜1p)}\label{discussion-limitations-and-future-research-1p}

tbd
