\select@language {english}
\contentsline {section}{Contents}{I}{section*.1}
\contentsline {section}{List of Abbreviations}{II}{section*.2}
\contentsline {section}{List of Figures}{III}{section*.4}
\contentsline {section}{List of Tables}{IV}{section*.5}
\contentsline {section}{\numberline {1}Introduction}{1}{section.1}
\contentsline {section}{\numberline {2}Foundations}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}Foundations of the Video Game Industry}{2}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Foundational Theories in Motive Research}{3}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Motives and Barriers: Literature Overview}{6}{subsection.2.3}
\contentsline {section}{\numberline {3}Development of a Conceptual Model}{14}{section.3}
\contentsline {subsection}{\numberline {3.1}Model Overview}{14}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Motives and Barriers: Literature Synthesis}{14}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Motives and Barriers: The Effects}{23}{subsection.3.3}
\contentsline {subsubsection}{\numberline {3.3.1}Extrovertive motives and their direct effects on Video Game Use}{23}{subsubsection.3.3.1}
\contentsline {subsubsection}{\numberline {3.3.2}Introvertive motives and their direct effects on Video Game Use}{25}{subsubsection.3.3.2}
\contentsline {subsubsection}{\numberline {3.3.3}Internal Barriers and their direct effects on Video Game Use}{27}{subsubsection.3.3.3}
\contentsline {subsubsection}{\numberline {3.3.4}External Barriers and their direct effects on Video Game Use}{28}{subsubsection.3.3.4}
\contentsline {subsubsection}{\numberline {3.3.5}Factorizing Motives and Barriers}{29}{subsubsection.3.3.5}
\contentsline {section}{\numberline {4}Model Validation}{29}{section.4}
\contentsline {subsection}{\numberline {4.1}Research Methodology}{29}{subsection.4.1}
\contentsline {subsubsection}{\numberline {4.1.1}Measuring the Dependent Variable}{29}{subsubsection.4.1.1}
\contentsline {subsubsection}{\numberline {4.1.2}Measuring the Independent Variables}{30}{subsubsection.4.1.2}
\contentsline {subsection}{\numberline {4.2}Sample Description}{32}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Results}{36}{subsection.4.3}
\contentsline {subsubsection}{\numberline {4.3.1}Reliability and Unidimensionality of Scales}{36}{subsubsection.4.3.1}
\contentsline {subsubsection}{\numberline {4.3.2}Testing of the Conceptual Model and its Hypotheses}{38}{subsubsection.4.3.2}
\contentsline {subsubsection}{\numberline {4.3.3}Testing the Factorized Model}{41}{subsubsection.4.3.3}
\contentsline {subsubsection}{\numberline {4.3.4}Post-hoc analysis - Gamer type as moderator?}{44}{subsubsection.4.3.4}
\contentsline {section}{\numberline {5}Conclusion}{46}{section.5}
\contentsline {subsection}{\numberline {5.1}Discussion of Results}{46}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Implications}{51}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Limitations and Future Research}{53}{subsection.5.3}
\contentsline {section}{References}{71}{section*.6}
\contentsline {section}{Appendices}{72}{section*.7}
\par 
\contentsline {section}{\numberline {A}The Gamer Types}{72}{Appendix.1.A}
\contentsline {subsection}{\numberline {A.1}The Core Gamer}{72}{subsection.1.A.1}
\contentsline {subsection}{\numberline {A.2}The Casual Gamer}{73}{subsection.1.A.2}
\contentsline {section}{\numberline {B}Additional Data}{73}{Appendix.1.B}
\contentsline {section}{Declaration of Academic Integrity}{76}{section*.8}
