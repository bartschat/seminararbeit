\cleardoublepage
\pagenumbering{arabic}
\setcounter{page}{1}

# Einführung 

* Relevanz des Themas Videospiele: Größe des Marktes darstellen, Größe der Zielgruppe Videospieler
* Definition Videogame-Konsole, Videogame-System, Plattform, etc.
* Abgrenzung des relevanten Marktes, der im Rahmen der Arbeit betrachtet wird (nur die letzten drei Generationen der "Großen Drei")
* Feststellung: Videospiele/-Konsolen sind ein hedonisches Produkt - Implikationen für Kaufverhalten der Konsumenten, Kaufkriterien im Vergleich zu uilitaristischen Produkten
* Sind Erfolgsfaktoren anderer hedonischer Produkte (teilweise) übertragbar?

## Darstellung von erfolgreichen bzw. nicht-erfolgreichen Konsolen

Tabelle mit Absatzzahlen und subjektiver Bewertung erfolgreich/nicht erfolgreich

Frage nach dem Gründen des Scheiterns der Wii U (Verwendung als Negativ-Beispiel) als Motivation für die Suche nach Erfolgsfaktoren für Videogame-Konsolen

## Thesen

* Übertragbarkeit von Erfogsfaktoren aus artverwandten, hedonischen Märkten, Bsp: Film (?)
* Die Rolle von Marken im Konsolenmarkt
* Leistungsfähigkeit der Hardware
* Verfügbarkeit und Qualität von Software (Indirekte Netzwerkeffekte?)
* Gute Wahl der Positionierung/Zielgruppen (Casual Gamer, Core Gamer (Definition))
* Preisgestaltung bei Markteinführung und über den Lebenszyklus hinweg
* Online-Dienste (XBox Live, Playstation Network, Nintendo Network)

# Erfolgsfaktoren in hedonischen Märkten

Vgl. Erfolgsfaktoren im Film, Übertragbar? 


# Leistungsfähigkeit der Hardware

* Herleitung, das Hardware eine relevante Rolle spielt, über Immersion durch realistische Grafik im Feld "narrative, player, gameplay"
* Ziel: Erzeugen eines Spieleerlebnisses
* Betrachtung der Leistungsfähigkeit der Konsolenhardware im Vergleich über drei Generationen (Arbeitsspeicher/RAM, GPU Durchsatz, maximale Auflösung, Größe (Speicher) der Spielemedien)
* Fehlende Hardwarekompetenz bei Nintendo? Andere Strategie - Erzeugung von Immersion bei Wii Nutzern durch Steuerungskonzept, aber: Generationswechsel zur Wii U...
* Lerneffekte/Vorteile für Microsoft und Sony beim Wechsel der Generation von X360/PS3 auf Xone/PS4 durch vorherige Generation, Bsp. Umgang mit HD-Grafik?
* Evtl: BluRay vs HD-DVD - Sind Marktstandards Erfolgsfaktor/kaufverhaltensrelevant?

\begin{figure}
\centering
\begin{tikzpicture} 
       \begin{axis} 
       	  [
    	     width=\linewidth*.65, % Scale the plot to \linewidth
              grid=major, 
              grid style={dashed,gray!30},
              xlabel=Konsolengeneration, % Set the labels
              ylabel=GFLOPS,
              legend pos = north west,
              x tick label style = {rotate = 0, anchor=north, /pgf/number format/1000 sep= },
              y tick label style = {/pgf/number format/1000 sep= },
              xtick distance = 1,
              minor x tick num = 1,
              ymin = 0, xmin = 5.5,
              ymax = 1900, xmax = 8.5,
             % title = Entwicklung der Leistungsfähigkeit - hier: GFLOPS der GPU,
              legend entries = {$Sony$, $Microsoft$, $Nintendo$}
          ]
           \addplot table [col sep=comma, x = Generation, y = GFLOPS]{GPU_Sony.csv};
		   \addplot table [col sep=comma, x = Generation, y = GFLOPS]{GPU_MS.csv};
		   \addplot table [col sep=comma, x = Generation, y = GFLOPS]{GPU_Nintendo.csv};
       \end{axis} 
    \end{tikzpicture}
    \caption{GPU GFLOPS Quelle: Eigene Darstellung}
    \label{abb:gpupower}
\end{figure} 

\begin{figure}
\centering
\begin{tikzpicture} 
       \begin{axis} 
       	  [
    	     width=\linewidth*.65, % Scale the plot to \linewidth
              grid=major, 
              grid style={dashed,gray!30},
              xlabel=Konsolengeneration, % Set the labels
              ylabel=Pixels,
              legend pos = north west,
              x tick label style = {rotate = 0, anchor=north, /pgf/number format/1000 sep= },
              y tick label style = {/pgf/number format/1000 sep= },
              xtick distance = 1,
              minor x tick num = 1,
              ymin = 0, xmin = 5.5,
              ymax = 9000000, xmax = 8.5,
             % title = ,
              legend entries = {$Sony$, $Microsoft$, $Nintendo$}
          ]
           \addplot table [col sep=comma, x = Generation, y = Pixels]{Pixel_Sony.csv};
		   \addplot table [col sep=comma, x = Generation, y = Pixels]{Pixel_MS.csv};
		   \addplot table [col sep=comma, x = Generation, y = Pixels]{Pixel_Nintendo.csv};
       \end{axis} 
    \end{tikzpicture}
    \caption{Maximale Auflösung Quelle: Eigene Darstellung}
    \label{abb:pixels}
\end{figure} 


\begin{center}
        \csvautotabular{GPU_all.csv}\cite{Cassamasina2006}
\end{center}
# Verfügbarkeit von Software - Quantität und Qualität

* Plattform ohne Spiele bietet nur eingeschränkten Nutzen (als Medienabspielgerät), Ableitung der indirekten und direkten Netzwerkeffekte
* Betrachtung des Launch Line Ups der Konsolen: Anzahl und Qualität, mögliche Korrelation mit Verkaufszahlen?
* Darstellung verfügbarer Titel über Lebensdauer, Anzahl und Qualität, Korrelation mit Absatzahlen der Plattformen?
* Betrachtung der mittleren Softwarequalität? (kumuliert?, jährlich?, monatlich?)
* Superstar-Software als System-Seller
* Exklusiv-Titel nicht rein als absatzsteigernd für eigene Plattform, sondern um Konkurrenz den Zugriff zu verwehren


# Positionierung der Konsole durch den Hersteller und Wahl der Zielgruppe

* Darstellung der Postionierung und Zielgruppen 
* Unterschiede der drei Hersteller
* Casual Gamer vs Core Gamer - Definitionen
* Welche Konsole für welchen Gamertyp? (Bsp: Wii für Casual und former non-Gamer = Erfolg, Wii U ohne Relevanz in diesem Segment...)
* Evtl: Vercasualisierung der Spieleszene als Einschub? (eher nicht!)

## Markenimages/Markenpositionierung als Erfolgsfaktor im Konsolenmarkt

* Welche Marken-Images haben die Konsolenhersteller und/oder ihre Konsolen? 
* Welche Implikationen ergeben sich aus den Markenimages für die Bearbeitung von Zielgruppen?


# Preisgestaltung als Erfolgsfaktor

* Wahl des initialen Preises bei Markteinführung 
* Skimming vs Penetration
* Subventionierung der Hardware, um mit der Software Profit zu erwirtschaften
* Wenn Software Profitquelle, ist dann Softwareentwicklung durch den Plattformhersteller nötig/sinnvoll?
* Entwicklung des Preises über den Lebenszyklus des Produktes/der Plattform

# Online-Dienste als Plattform-Seller

* Kundenbindung durch PSN, XBox Live durch Nutzung von Multiplayer-Games?
* Mehrwert für den Kunden
* Soziales Erlebnis (Wettbewerb, gemeinsames Erlebnis)
* Verursachung von Switching Costs beim Wechsel zu einem anderen Hersteller bei einem Generationswechsel oder während einer Generation?
* Evtl. Vergleich erfolgreicher Multiplayer-Titel: Mario Kart 8 (Wii U) vs FIFA 16 (PS4), Absatzzahlen, Online-Nutzer (falls verfügbare Daten), Auswirkungen auf Hardware Sales? (Daten??)
* Anforderungen an Online-Dienste:
	* Stabilität
	* Soziale Interaktion
	* Selbstdarstellung - Achievments
	* Multiplayer
* Einnahmen für den Plattformbetreiber
* Anti Piracy?

#Fazit 

Offen... 
