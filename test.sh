#!/bin/bash

if [ $# -eq 2 ]
	then
        	if [ "$1" == "push" ]
                	then
                        	date=`date +"%y-%m-%d:%H%M"`
				echo "$1 $2"
                	else	
				echo "$1"
        	fi
else
        echo "Not pushed to git repo!"
fi
