\clearpage
\section*{Abkürzungsverzeichnis}
\addcontentsline{toc}{section}{Abkürzungsverzeichnis}

\begin{tabular}{ll}
	bzgl.	&	bezüglich\\
	bzw.	&	beziehungsweise\\
	et al.	&	et alii (und andere)\\
	etc.	&	et cetera (und so weiter)\\
	f., ff.	&	folgende Seite(n)\\
	ggf.	&	gegebenenfalls\\
	Hrsg.	&	Herausgeber\\
	Mio.	&	Millionen\\
	No.	&	number (Nummer)\\
	o.	&	oben\\
	s.	&	siehe\\
	S.	&	Seite(n)\\
	vgl.	&	vergleiche\\
	Vol.	&	Volume (Band)\\
	z.B.	&	zum Beispiel\\
\end{tabular}
Die in dieser Arbeit erwähnten Unternehmens-, Produkt oder 
Markenbezeichnungen können Marken oder eingetragene 
Markenzeichen der jeweiligen Eigentümer sein. Die Wiedergabe 
von Marken- und/oder Warenzeichen in dieser Arbeit berechtigt 
nicht zu der Annahme, dass diese als frei von Rechten Dritter 
zu betrachten seien. Alle erwähnten Marken- und/oder Warenzeichen 
unterliegen uneingeschränkt den länderspezifischen Schutzbestimmungen 
und den Besitzrechten der jeweiligen eingetragenen Eigentümer.
