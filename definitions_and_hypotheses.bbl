\begin{thebibliography}{}

\bibitem[11bit Studios, 2014]{11bit2014}
11bit Studios (2014).
\newblock {This War of Mine}.
\newblock Available from: \url{http://www.thiswarofmine.com/}.

\bibitem[Adams, 2000]{Adams2000}
Adams, E. (2000).
\newblock {Casual Versus Core}.
\newblock Available from:
  \url{http://www.gamasutra.com/view/feature/3120/casual{\_}versus{\_}core.php}.

\bibitem[Adams, 2002]{Adams2002}
Adams, E. (2002).
\newblock {From Casual to Core: A Statistical Mechanism for Studying Gamer
  Dedication}.
\newblock Available from:
  \url{http://www.gamasutra.com/view/feature/131397/from{\_}casual{\_}to{\_}core{\_}a{\_}statistical{\_}.php?print=1}.

\bibitem[Baard et~al., 2004]{Baard2004}
Baard, P.~P., Deci, E.~L., and Ryan, R.~M. (2004).
\newblock {Intrinsic Need Satisfaction: A Motivational Basis of Performance and
  Well-Being in Two Work Settings}.
\newblock {\em Journal of Applied Social Psychology}, 34(10):2045--2068.
\newblock Available from:
  \url{http://onlinelibrary.wiley.com/doi/10.1111/j.1559-1816.2004.tb02690.x/abstract}.

\bibitem[BadMouth, 2012]{BadMouth2012}
BadMouth (2012).
\newblock {crosbred900's Analog Xbox360 Control Panel - DONE!}
\newblock Available from:
  \url{http://forum.arcadecontrols.com/index.php?topic=128941.0}.

\bibitem[Barefoot, 2013]{Barefoot2013}
Barefoot, H. (2013).
\newblock {Escapist Editorials - In Defense of the Casual Gamer}.
\newblock Available from:
  \url{http://www.escapistmagazine.com/articles/view/video-games/editorials/10699-In-Defense-of-the-Casual-Gamer}.

\bibitem[Bartle, 1999]{Bartle1999}
Bartle, R. (1999).
\newblock {Players Who Suit MUDs}.
\newblock Available from: \url{http://mud.co.uk/richard/hcds.htm}.

\bibitem[Bateman et~al., 2011]{Bateman2011}
Bateman, C., Lowenhaupt, R., and Nacke, L.~E. (2011).
\newblock {Player Typology in Theory and Practice}.
\newblock {\em Proceedings of DiGRA 2011 Conference: Think Design Play.}, pages
  1--24.

\bibitem[Billieux et~al., 2013]{Billieux2013}
Billieux, J., {Van Der Linden}, M., Achab, S., Khazaal, Y., Paraskevopoulos,
  L., Zullino, D., and Thorens, G. (2013).
\newblock {Why do you play World of Warcraft? An in-depth exploration of
  self-reported motivations to play online and in-game behaviours in the
  virtual world of Azeroth}.
\newblock {\em Computers in Human Behavior}.

\bibitem[Boyle et~al., 2012]{Boyle2012}
Boyle, E.~A., Connolly, T.~M., Hainey, T., and Boyle, J.~M. (2012).
\newblock {Engagement in digital entertainment games: A systematic review}.
\newblock {\em Computers in Human Behavior}, 28(3):771--780.
\newblock Available from: \url{http://dx.doi.org/10.1016/j.chb.2011.11.020}.

\bibitem[Budd, 1998]{budd_aesthetics_1998}
Budd, M. (1998).
\newblock {Aesthetics}.
\newblock {\em Routledge Encyclopedia of Philosophy}.
\newblock Available from:
  \url{https://www.rep.routledge.com/articles/overview/aesthetics/v-1}.

\bibitem[Carmichael, 2011]{Carmichael2011}
Carmichael, S. (2011).
\newblock {Five Reasons Why We Stop Playing Video Games}.
\newblock Available from:
  \url{http://www.gamezone.com/originals/five-reasons-why-we-stop-playing-video-games}.

\bibitem[Chernev et~al., 2014]{Chernev2014}
Chernev, A., B{\"{o}}ckenholt, U., and Goodman, J. (2014).
\newblock {Choice overload: A conceptual review and meta-analysis}.
\newblock {\em Journal of Consumer Psychology}.
\newblock Available from: \url{http://dx.doi.org/10.1016/j.jcps.2014.08.002}.

\bibitem[Cheyne et~al., 2006]{Cheyne2006}
Cheyne, J.~A., Carriere, J. S.~A., and Smilek, D. (2006).
\newblock {Absent-mindedness: Lapses of conscious awareness and everyday
  cognitive failures}.
\newblock {\em Consciousness and Cognition}, 15(3):578--592.

\bibitem[Deci and Ryan, 2000]{Deci2000}
Deci, E.~L. and Ryan, R.~M. (2000).
\newblock {The " What " and " Why " of Goal Pursuits : Human Needs and the
  Self-Determination of Behavior}.
\newblock {\em Psychological Inquiry}, 11(4):37--41.

\bibitem[Ferguson et~al., 2014]{Ferguson2014}
Ferguson, C.~J., Olson, C.~K., Kutner, L.~a., and Warner, D.~E. (2014).
\newblock {Violent Video Games, Catharsis Seeking, Bullying, and Delinquency: A
  Multivariate Analysis of Effects}.
\newblock {\em Crime {\&} Delinquency}, 60(5):764--784.

\bibitem[Freitas, 2006]{Freitas2006}
Freitas, S.~D. (2006).
\newblock {Learning in Immersive worlds: A review of game-based learning}.
\newblock Technical report, JISC.

\bibitem[Gandasegui, 2010]{Gandasegui2010}
Gandasegui, D. (2010).
\newblock {The non-gamer}.
\newblock Technical report.

\bibitem[Gert and Gert, 2016]{Gert2016}
Gert, B. and Gert, J. (2016).
\newblock {Stanford Encyclopedia of Philosophy Archive}.
\newblock Available from:
  \url{https://plato.stanford.edu/archives/spr2016/entries/morality-definition/}.

\bibitem[Hall-Stigerts, 2013]{Hall-Stigerts2015}
Hall-Stigerts, L. (2013).
\newblock {What does it Mean to be a Gamer}.
\newblock Available from:
  \url{http://www.bigfishgames.com/blog/what-does-it-mean-to-be-a-gamer/}.

\bibitem[Hamari and Tuunanen, 2014]{Hamari2014}
Hamari, J. and Tuunanen, J. (2014).
\newblock {Player Types: A Meta-synthesis}.
\newblock {\em Transactions of the Digital Games Research Association},
  1(2):29--53.
\newblock Available from:
  \url{http://todigra.org/index.php/todigra/article/view/13/19}.

\bibitem[Hennion, 2007]{Hennion2007}
Hennion, A. (2007).
\newblock {Those Things That Hold Us Together: Taste and Sociology}.
\newblock {\em Cultural Sociology}, 1(1):97--114.
\newblock Available from: \url{http://cus.sagepub.com/content/1/1/97.abstract}.

\bibitem[Henry et~al., 2014]{Henry2014}
Henry, S.~L., Abou-Zahra, S., and Brewer, J. (2014).
\newblock {The role of accessibility in a universal web}.
\newblock In {\em Proceedings of the 11th Web for All Conference on - W4A '14},
  pages 1--4.
\newblock Available from:
  \url{http://dl.acm.org/citation.cfm?id=2596695.2596719}.

\bibitem[Hilgard et~al., 2013]{Hilgard2013}
Hilgard, J., Engelhardt, C.~R., and Bartholow, B.~D. (2013).
\newblock {Individual differences in motives, preferences, and pathology in
  video games: The gaming attitudes, motives, and experiences scales (GAMES)}.
\newblock {\em Frontiers in Psychology}, 4(SEP):1--13.

\bibitem[Holmes, 2017]{Holmes2017}
Holmes, C. (2017).
\newblock 5 reasons why you don't like video games.
\newblock Available from:
  \url{http://pixelkin.org/2014/09/18/5-reasons-why-you-dont-like-video-games/}.

\bibitem[Homburg et~al., 2005]{Homburg2005}
Homburg, C., Koschate, N., and Hoyer, W.~D. (2005).
\newblock {Do Satisfied Customers Really Pay More? A Study of the Relationship
  between Customer Satisfaction and Willingness to Pay}.
\newblock {\em Journal of Marketing}, 69(2):84--96.

\bibitem[IGDA, 2006]{IGDA2006}
IGDA (2006).
\newblock {2006 Casual Games White Paper}.
\newblock Available from:
  \url{archives.igda.org/casual/IGDA{\_}CasualGames{\_}Whitepaper{\_}2006.pdf}.

\bibitem[Iyengar and Lepper, 2000]{Iyengar2000}
Iyengar, S.~S. and Lepper, M.~R. (2000).
\newblock {When Choice is Demotivating: Can One Desire Too Much of a Good
  Thing?}
\newblock {\em Journal of Personality and Social Psychology}, 79(6):995--1006.

\bibitem[Juul, 2010]{Juul2010}
Juul, J. (2010).
\newblock {\em {A Casual Revolution - Reinventing Video Games and their
  Players}}.
\newblock The MIT Press, London, England.
\newblock Available from:
  \url{http://scholar.google.com/scholar?hl=en{\&}btnG=Search{\&}q=intitle:A+CASUAL+REVOLUTION{\#}1{\%}5Cnhttp://scholar.google.com/scholar?hl=en{\&}btnG=Search{\&}q=intitle:A+casual+revolution{\#}1}.

\bibitem[Kahn et~al., 2015]{Kahn2015}
Kahn, A.~S., Shen, C., Lu, L., Ratan, R.~A., Coary, S., Hou, J., Meng, J.,
  Osborn, J., and Williams, D. (2015).
\newblock {The Trojan Player Typology: A cross-genre, cross-cultural,
  behaviorally validated scale of video game play motivations}.
\newblock {\em Computers in Human Behavior}.

\bibitem[Kallio et~al., 2011]{Kallio2011}
Kallio, K.~P., M{\"{a}}yr{\"{a}}, F., and Kaipainen, K. (2011).
\newblock {At Least Nine Ways to Play: Approaching Gamer Mentalities}.
\newblock {\em Games and Culture}, 6(4):327--353.
\newblock Available from:
  \url{http://journals.sagepub.com/doi/10.1177/1555412010391089}.

\bibitem[Kaminsky, 2016]{Kaminsky2016}
Kaminsky, H. (2016).
\newblock {Xbox one elite controller provides a better option for gamers with
  disabilities}.
\newblock Available from:
  \url{https://www.digitaltrends.com/gaming/xbox-one-elite-controller-disability-affordable-accessible/}.

\bibitem[Kaye and Bryce, 2012]{Kaye2012}
Kaye, L.~K. and Bryce, J. (2012).
\newblock {Putting the"fun factor" into gaming: The influence of social
  contexts on experiences of playing videogames}.
\newblock {\em International Journal of Internet Science}, 7(1):23--37.

\bibitem[Kiili, 2005]{Kiili2005}
Kiili, K. (2005).
\newblock {Digital game-based learning: Towards an experiential gaming model}.
\newblock {\em The Internet and Higher Education}, 8:13--24.

\bibitem[Klimmt and Hartmann, 2006]{Klimmt2006}
Klimmt, C. and Hartmann, T. (2006).
\newblock {Effectance, self-efficacy, and the motivation to play video games}.
\newblock {\em Playing video games: motives, responses, and consequences},
  (December):133--145.
\newblock Available from:
  \url{http://0-search.ebscohost.com.mercury.concordia.ca/login.aspx?direct=true{\&}db=psyh{\&}AN=2006-05034-026{\&}site=ehost-live{\%}5Cnhttp://scholar.google.com/scholar?hl=en{\&}btnG=Search{\&}q=intitle:Video+Games+for+Entertainment+and+Education{\#}0}.

\bibitem[Koerth-Baker, 2016]{Koerth-Baker2016}
Koerth-Baker, M. (2016).
\newblock {Why Boredom Is Anything but Boring}.
\newblock Available from:
  \url{https://www.scientificamerican.com/article/why-boredom-is-anything-but-boring/}.

\bibitem[K{\"{u}}hn et~al., 2014]{Kuehn2014}
K{\"{u}}hn, S., Gleich, T., Lorenz, R., Lindenberger, U., and Gallinat, J.
  (2014).
\newblock {Playing Super Mario induces structural brain plasticity: gray matter
  changes resulting from training with a commercial video game}.
\newblock {\em Molecular Psychiatry}, 19:265--271.

\bibitem[Kuittinen et~al., 2007]{Kuittinen2007}
Kuittinen, J., Kultima, A., Niemel{\"{a}}, J., and Paavilainen, J. (2007).
\newblock {Casual games discussion}.
\newblock In {\em Proceedings of the 2007 conference on Future Play Future Play
  07}, number October 2016, pages 105--112.
\newblock Available from:
  \url{http://portal.acm.org/citation.cfm?doid=1328202.1328221{\%}5Cnhttp://dl.acm.org.elib.tcd.ie/citation.cfm?id=1328221{\&}CFID=162482133{\&}CFTOKEN=64520688}.

\bibitem[Kultima, 2009]{Kultima2009}
Kultima, A. (2009).
\newblock {Casual game design values}.
\newblock {\em Proceedings of the 13th International MindTrek Conference:
  Everyday Life in the Ubiquitous Era on - MindTrek '09}, (October):58.
\newblock Available from:
  \url{http://portal.acm.org/citation.cfm?doid=1621841.1621854}.

\bibitem[Laffan et~al., 2016]{Laffan2016}
Laffan, D.~A., Greaney, J., Barton, H., and Kaye, L.~K. (2016).
\newblock {The relationships between the structural video game characteristics,
  video game engagement and happiness among individuals who play video games}.
\newblock {\em Computers in Human Behavior}.

\bibitem[Lane et~al., 1999]{Lane1999}
Lane, R.~D., Chua, P. M.-L., and Dolan, R.~D. (1999).
\newblock {Common effects of emotional valence, arousal and attention on neural
  activation during visual processing of pictures.pdf}.
\newblock {\em Neuropsychologia}, 37:989--997.

\bibitem[Lazzaro, 2004]{Lazzaro2004}
Lazzaro, N. (2004).
\newblock {Why We Play Games: Four Keys to More Emotion Without Story}.
\newblock Technical report.
\newblock Available from:
  \url{http://xeodesign.com/xeodesign{\_}whyweplaygames.pdf}.

\bibitem[Leigh, 2011]{Leigh2011}
Leigh, A. (2011).
\newblock {Why Don't I Lose Myself In Games Anymor}.

\bibitem[Leversen et~al., 2012]{Leversen2012}
Leversen, I., Danielsen, A.~G., Birkeland, M.~S., and Samdal, O. (2012).
\newblock {Basic Psychological Need Satisfaction in Leisure Activities and
  Adolescents' Life Satisfaction}.
\newblock {\em Journal of Youth and Adolescence}, 41(12):1588--1599.

\bibitem[Li et~al., 2011]{Li2011}
Li, D., Liau, A., and Khoo, A. (2011).
\newblock {Examining the influence of actual-ideal self-discrepancies,
  depression, and escapism, on pathological gaming among massively multiplayer
  online adolescent gamers.}
\newblock {\em Cyberpsychology, behavior and social networking},
  14(9):535--539.

\bibitem[Linke, 2014]{Linke2014}
Linke, D. (2014).
\newblock {Keine Maus, kein Problem}.
\newblock Available from:
  \url{http://www.zeit.de/digital/games/2014-11/videospiele-behinderung-quadstick-jouse/komplettansicht}.

\bibitem[Lovato, 2015]{Lovato2015}
Lovato, N. (2015).
\newblock {16 Reasons why players are leaving your game}.
\newblock Available from:
  \url{http://blog.gameanalytics.com/blog/16-reasons-players-leaving-game.html}.

\bibitem[MacLeod, 2016]{MacLeod2016}
MacLeod, R. (2016).
\newblock {The Things That Shape Our Taste in Video Games}.
\newblock Available from:
  \url{http://kotaku.com/the-things-that-shape-our-taste-in-video-games-1762898863}.

\bibitem[Maslow, 1943a]{Maslow1943}
Maslow, A. (1943a).
\newblock {A theory of human motivation}.
\newblock {\em Psychological Review}, 50(4):370--396.

\bibitem[Maslow, 1943b]{Maslow1943a}
Maslow, A.~H. (1943b).
\newblock {a Theory of Human Motivation}.
\newblock {\em Psychological Review}, 50(4):370--396.

\bibitem[McClelland, 1987]{McClelland1987}
McClelland, D.~C. (1987).
\newblock {\em {Human Motivation}}.

\bibitem[Merriam-Webster, 1995]{Merriam-Webster1995}
Merriam-Webster (1995).
\newblock {\em {Merriam-Webster's Encyclopedia of Literature}}.

\bibitem[Merriam-Webster, 2017a]{MerriamWebsterBarrier2017}
Merriam-Webster (2017a).
\newblock {Barrier | Definition of Barrier by Merriam-Webster}.
\newblock Available from:
  \url{https://www.merriam-webster.com/dictionary/barrier}.

\bibitem[Merriam-Webster, 2017b]{Merriam-Webster2017}
Merriam-Webster (2017b).
\newblock {Definition of Accessibility by Merriam-Webster}.
\newblock Available from:
  \url{https://www.merriam-webster.com/dictionary/accessibility}.

\bibitem[Mick et~al., 2017]{Mick2017}
Mick, D.~G., Broniarczyk, S.~M., and Haidt, J. (2017).
\newblock {Choose, Choose, Choose, Choose, Choose, Choose, Choose: Emerging and
  Prospective Research on the Deleterious Effects of Living in Consumer
  Hyperchoic}.
\newblock {\em Journal of Business Ethics}, 52(2):207--211.

\bibitem[Murray, 1999]{Murray1999}
Murray, J. (1999).
\newblock {Hamlet on The Holodeck: The Future of Narrative In Cyberspace.}
\newblock {\em MFS Modern Fiction Studies}, 45(2):553--556.

\bibitem[Nacke et~al., 2014]{Nacke2014}
Nacke, L.~E., Bateman, C., and Mandryk, R.~L. (2014).
\newblock {BrainHex: A neurobiological gamer typology survey}.
\newblock {\em Entertainment Computing}, 5(1):55--62.
\newblock Available from: \url{http://dx.doi.org/10.1016/j.entcom.2013.06.002}.

\bibitem[Nacke and Lindley, 2008]{Nacke2008}
Nacke, L.~E. and Lindley, C.~A. (2008).
\newblock {Flow and Immersion in First-person Shooters: Measuring the Player's
  Gameplay Experience}.
\newblock In {\em Proceedings of the 2008 Conference on Future Play: Research,
  Play, Share}, pages 81--88.
\newblock Available from:
  \url{http://arxiv.org/abs/1004.0248{\%}5Cnhttp://doi.acm.org/10.1145/1496984.1496998{\%}5Cnhttp://dl.acm.org/citation.cfm?id=1496998}.

\bibitem[Nelson, 1970]{Nelson1970}
Nelson, P. (1970).
\newblock {Information and Consumer Behavior}.
\newblock {\em Journal of Political Economy}, 78(2):311--329.
\newblock Available from: \url{https://doi.org/10.1086/259630}.

\bibitem[Nintendo, 2011]{Iwata2011}
Nintendo (2011).
\newblock {Nintendo E3 2011}.
\newblock Available from:
  \url{http://iwataasks.nintendo.com/interviews/index.html?disableNav=true/{\#}/e32011/newhw/0/6}.

\bibitem[Oliver et~al., 2016]{Oliver2016}
Oliver, M.~B., Bowman, N.~D., Woolley, J.~K., Rogers, R., Sherrick, B.~I., and
  Chung, M.-Y. (2016).
\newblock {Video games as meaningful entertainment experiences.}
\newblock {\em Psychology of Popular Media Culture}, 5(4):390--405.
\newblock Available from:
  \url{http://doi.apa.org/getdoi.cfm?doi=10.1037/ppm0000066}.

\bibitem[Pardee, 1990]{Pardee1990}
Pardee, R.~L. (1990).
\newblock {Motivation Theories of Maslow, Herzberg, McGregor {\&} McClelland. A
  Literature Review of Selected Theories Dealing with Job Satisfaction and
  Motivation}.
\newblock Technical report.

\bibitem[Porter, 2017]{Porter2017}
Porter, M. (2017).
\newblock 7 normal, everyday things that are impossible to explain to
  non-gamers.
\newblock Available from:
  \url{http://www.gamesradar.com/7-normal-everyday-things-are-impossible-explain-non-gamers/{\#}comment-jump}.

\bibitem[Przybylski et~al., 2010]{Przybylski2010}
Przybylski, A.~K., Rigby, C.~S., and Ryan, R.~M. (2010).
\newblock {A motivational model of video game engagement.}
\newblock {\em Review of General Psychology}, 14(2):154.
\newblock Available from: \url{http://psycnet.apa.org/journals/gpr/14/2/154/}.

\bibitem[Qin et~al., 2009]{Qin2009}
Qin, H., {Patrick Rau}, P.-L., and Salvendy, G. (2009).
\newblock {Measuring Player Immersion in the Computer Game Narrative}.
\newblock {\em International Journal of Human-Computer Interaction},
  25(2):107--133.
\newblock Available from:
  \url{http://www.informaworld.com/openurl?genre=article{\&}doi=10.1080/10447310802546732{\&}magic=crossref}.

\bibitem[Reinecke, 2009]{Reinecke2009}
Reinecke, L. (2009).
\newblock {Games and Recovery: The Use of Video and Computer Games to
  Recuperate from Stress and Strain}.
\newblock {\em Journal of Media Psychology}, 21:126--142.

\bibitem[Ryan et~al., 2006]{Ryan2006}
Ryan, R.~M., Rigby, C.~S., and Przybylski, A. (2006).
\newblock {The motivational pull of video games: A self-determination theory
  approach}.
\newblock {\em Motivation and Emotion}, 30(4):347--363.

\bibitem[Scharkow et~al., 2015]{Scharkow2015}
Scharkow, M., Festl, R., Vogelgesang, J., and Quandt, T. (2015).
\newblock {Beyond the "core-gamer": Genre preferences and gratifications in
  computer games}.
\newblock {\em Computers in Human Behavior}, 44:293--298.

\bibitem[Scheibehenne, 2008]{Scheibehenne2008}
Scheibehenne, B. (2008).
\newblock {\em {The Effect of Having Too Much Choice}}.
\newblock Ph.d. thesis, Humboldt-Universtit{\"{a}}t zu Berlin.

\bibitem[Schellong, 2014]{Schellong2014a}
Schellong, M. (2014).
\newblock {Weniger von wenig ist nicht mehr „This War of Mine“ und der
  Mangel an Wahlfreiheit (eine Rezension)PAIDIA}.
\newblock Available from: \url{http://www.paidia.de/?p=4216}.

\bibitem[Shaw, 2010]{Shaw2010}
Shaw, A. (2010).
\newblock {What is video game culture? Cultural studies and game studies}.
\newblock {\em Games and Culture}, 5(4):403--424.
\newblock Available from:
  \url{http://gac.sagepub.com/cgi/doi/10.1177/1555412009360414}.

\bibitem[Sherry et~al., 2006]{Sherry2006}
Sherry, J.~L., Lucas, K., Greenberg, B., and Lachlan, K. (2006).
\newblock {Video Game Uses and Gratifications as Predictors of Use and Game
  Preference}.
\newblock {\em Playing Computer Games: Motives, Responses, and Consequences},
  (January):213--224.

\bibitem[Simon et~al., 2017]{Simon2017}
Simon, H., Clausen, G., and Tackle, G. (2017).
\newblock {Gabler Wirtschaftslexikon, Stichwort: Preisbereitschaft}.
\newblock Available from:
  \url{http://wirtschaftslexikon.gabler.de/Archiv/13898/preisbereitschaft-v7.html}.

\bibitem[Statista, 2015]{Statista2015}
Statista (2015).
\newblock {Anteil der Haushalte* mit Computer in ausgew{\"{a}}hlten
  L{\"{a}}ndern in Europa in den Jahren 2013 und 2015}.
\newblock Available from:
  \url{https://de.statista.com/statistik/daten/studie/20140/umfrage/anteil-der-haushalte-mit-pc-in-ausgewaehlten-laendern/}.

\bibitem[Statista, 2017]{Statista2017}
Statista (2017).
\newblock {Anteil der Smartphone-Nutzer in Deutschland in den Jahren 2013 bis
  2017}.
\newblock Available from:
  \url{https://de.statista.com/statistik/daten/studie/585883/umfrage/anteil-der-smartphone-nutzer-in-deutschland/}.

\bibitem[SteamSpy, 2017]{SteamSpy2017}
SteamSpy (2017).
\newblock {SteamSpy}.
\newblock Available from: \url{https://steamspy.com/}.

\bibitem[Stewart, 2011]{Stewart2011}
Stewart, B. (2011).
\newblock {Personality And Play Styles: A Unified Model}.
\newblock Available from:
  \url{http://www.gamasutra.com/view/feature/6474/personality{\_}and{\_}play{\_}styles{\_}a{\_}.php}.

\bibitem[Tseng, 2011]{Tseng2011}
Tseng, F.~C. (2011).
\newblock {Segmenting online gamers by motivation}.
\newblock {\em Expert Systems with Applications}, 38(6):7693--7697.
\newblock Available from: \url{http://dx.doi.org/10.1016/j.eswa.2010.12.142}.

\bibitem[Vaughn, 2015]{Vaughn2015}
Vaughn, R.~C. (2015).
\newblock {Aggression Predictors in Video Games : Is Catharsis to Blame ?}
\newblock {\em Thesis}.

\bibitem[Vorderer et~al., 2003]{Vorderer2003}
Vorderer, P., Hartmann, T., and Klimmt, C. (2003).
\newblock {Explaining the enjoyment of playing video games: the role of
  competition}.
\newblock In {\em ICEC '03 Proceedings of the second international conference
  on Entertainment computing}, pages 1--9.

\bibitem[Weibel and Wissmath, 2011]{Weibel2011}
Weibel, D. and Wissmath, B. (2011).
\newblock {Immersion in computer games: The role of spatial presence and flow}.

\bibitem[Wikipedia, 2017]{Wikipedia2017}
Wikipedia (2017).
\newblock {Video game culture}.
\newblock Available from:
  \url{https://en.wikipedia.org/wiki/Video{\_}game{\_}culture}.

\bibitem[Williams, 2002]{Williams2002}
Williams, D. (2002).
\newblock {Structure and competition in the U . S . home video game industry}.
\newblock {\em International Journal on Media Management}, 4(1):41--54.

\bibitem[Yee, 2007]{Yee2007}
Yee, N. (2007).
\newblock {Motivations of Play in Online Games}.
\newblock {\em Journal of CyberPsychology and Behavior}, 9:772--775.

\bibitem[Zangwill, 2014]{Zangwill2014}
Zangwill, N. (2014).
\newblock {Aesthetic Judgment}.
\newblock {\em Stanford Encyclopedia of Philosophy}, pages 1--28.

\bibitem[Zeigler-Hill and Monica, 2015]{Zeigler-Hill2015}
Zeigler-Hill, V. and Monica, S. (2015).
\newblock {The HEXACO model of personality and video game preferences}.
\newblock {\em Entertainment Computing}, 11:21--26.
\newblock Available from: \url{http://dx.doi.org/10.1016/j.entcom.2015.08.001}.

\bibitem[Zillmann, 1988]{Zillmann1988}
Zillmann, D. (1988).
\newblock {Mood Management Through Communication Choices}.
\newblock {\em American Behavioral Scientist}, 31(3):327--340.

\end{thebibliography}
