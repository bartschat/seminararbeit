\cleardoublepage

#Eidesstattliche Erklärung

Ich erkläre hiermit, dass ich meine \Arbeitsart mit dem Titel

"\Arbeitstitel"

selbstständig und ohne fremde Hilfe angefertigt habe, und dass ich alle 
von anderen Autoren wörtliche übernommenen Stellen wie auch die sich an die 
Gedankengänge anderer Autoren eng anlehnenden Ausführungen meiner Arbeit
besonders gekennzeichnet und die Quellen zitiert habe.

\vspace{3cm}
\Ort, den \Abgabedatum
