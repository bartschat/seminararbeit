### SNIPPETS
	\begin{figure}

		\begin{tikzpicture}
	    	  \begin{axis}
	       	  [
	    	     width=\linewidth*.8,
	             grid=major,
	             grid style={dashed,gray!30},
	             xlabel=Konsolengeneration, % Set the labels
	             ylabel=Pixel,
	             legend pos = north west,
	             x tick label style = {rotate = 0, anchor=north, /pgf/number format/1000 sep= },
	             y tick label style = {/pgf/number format/1000 sep= },
	             xtick distance = 1,
	             minor x tick num = 1,
				 ymin = 0, xmin = 5.5,
	             ymax = 8000000, xmax = 8,
				 legend entries = {$Sony$, $Microsoft$, $Nintendo$}

	          ]
				  \addplot table [col sep=comma, x = Generation, y = Pixels]{Pixel_Sony.csv};
				  \addplot table [col sep=comma, x = Generation, y = Pixels]{Pixel_MS.csv};
				  \addplot table [col sep=comma, x = Generation, y = Pixels]{Pixel_Nintendo.csv};
			  \end{axis}
	    \end{tikzpicture}

	    \caption{Entwicklung der Leistungsfähigkeit von Konsolen - hier maximale Auflösung Quelle: Eigene Darstellung, Daten: Fussnote!}
	    \label{abb:pixels}

	\end{figure}





\begin{figure}
\begin{tikzpicture}
       \begin{axis}
       	  [
    	     width=\linewidth*.5, % Scale the plot to \linewidth
              grid=major,
              grid style={dashed,gray!30},
              xlabel=Konsolengeneration, % Set the labels
              ylabel=Pixels,
              %x unit=\si{\volt}, % Set the respective units
              %y unit=\si{\ampere},
              legend pos = north west,
              %x tick label style={rotate=90,anchor=east},
              x tick label style = {rotate = 0, anchor=east, /pgf/number format/1000 sep= },
              y tick label style = {/pgf/number format/1000 sep= },
              xtick distance = 1,
              minor x tick num = 1,
              ymin = 0, xmin = 6,
              ymax = 10000000, xmax = 8,
             % title = Entwicklung der Leistungsfähigkeit - hier: maximale Auflösung,
              legend entries = {$Sony$, $Microsoft$, $Nintendo$},
          ]
           \addplot table [col sep=comma, x = Generation, y = GFLOPS]{Pixel_Sony.csv};
		   \addplot table [col sep=comma, x = Generation, y = GFLOPS]{Pixel_MS.csv};
		   \addplot table [col sep=comma, x = Generation, y = GFLOPS]{Pixel_Nintendo.csv};
       \end{axis}
    \end{tikzpicture}
    \caption{Entwicklung der Leistungsfähigkeit von Konsolen - hier maximale Auflösung Quelle: Eigene Darstellung, Daten: Fussnote!}
    \label{abb:aufloesung}
\end{figure}


## Dynamische Diagramme

Bitte beachten Sie Abbildung \link{abb:geekbench} mit einer schicken Darstellung.

\begin{figure}
\begin{tikzpicture}
       \begin{axis}
       	  [
    	     width=\linewidth*.9, % Scale the plot to \linewidth
              grid=major,
              grid style={dashed,gray!30},
              xlabel=Release, % Set the labels
              ylabel=Geekbench Score,
              %x unit=\si{\volt}, % Set the respective units
              %y unit=\si{\ampere},
              legend pos = north west,
              %x tick label style={rotate=90,anchor=east},
              x tick label style = {rotate = 90, anchor=east, /pgf/number format/1000 sep= },
              y tick label style = {/pgf/number format/1000 sep= },
              xtick distance = 1,
              minor x tick num = 1,
              ymin = 0, xmin = 2009,
              ymax = 4500, xmax = 2016,
             % title = Entwicklung der Leistungsfähigkeit von Smartphones,
              legend entries = {$Apple$, $Samsung$},
          ]
           \addplot table [col sep=comma, x = Release, y = Score]{geekbench_apple.csv};
           \addplot table [col sep=comma, x = Release, y = Score]{geekbench_samsung.csv};
       \end{axis}
    \end{tikzpicture}
    \caption{Entwicklung der Leistungsfähigkeit von Smartphones Quelle: Eigene Darstellung, Daten: Primate Labs}
    \label{abb:geekbench}
\end{figure}
### Automatisch generierte Tabellen

\begin{center}
        \csvautotabular{geekbench_apple.csv}
        \csvautotabular{geekbench_samsung.csv}
\end{center}

## GPU Diagramme


\begin{figure}
\centering
\begin{tikzpicture}
       \begin{axis}
       	  [
    	     width=\linewidth*.65, % Scale the plot to \linewidth
              grid=major,
              grid style={dashed,gray!30},
              xlabel=Konsolengeneration, % Set the labels
              ylabel=GFLOPS,
              legend pos = north west,
              x tick label style = {rotate = 0, anchor=north, /pgf/number format/1000 sep= },
              y tick label style = {/pgf/number format/1000 sep= },
              xtick distance = 1,
              minor x tick num = 1,
              ymin = 0, xmin = 5.5,
              ymax = 1900, xmax = 8.5,
             % title = Entwicklung der Leistungsfähigkeit - hier: GFLOPS der GPU,
              legend entries = {$Sony$, $Microsoft$, $Nintendo$}
          ]
           \addplot table [col sep=comma, x = Generation, y = GFLOPS]{GPU_Sony.csv};
		   \addplot table [col sep=comma, x = Generation, y = GFLOPS]{GPU_MS.csv};
		   \addplot table [col sep=comma, x = Generation, y = GFLOPS]{GPU_Nintendo.csv};
       \end{axis}
    \end{tikzpicture}
    \caption{GPU GFLOPS Quelle: Eigene Darstellung}
    \label{abb:gpupower}
\end{figure}

\begin{figure}
\centering
\begin{tikzpicture}
       \begin{axis}
       	  [
    	     width=\linewidth*.65, % Scale the plot to \linewidth
              grid=major,
              grid style={dashed,gray!30},
              xlabel=Konsolengeneration, % Set the labels
              ylabel=Pixels,
              legend pos = north west,
              x tick label style = {rotate = 0, anchor=north, /pgf/number format/1000 sep= },
              y tick label style = {/pgf/number format/1000 sep= },
              xtick distance = 1,
              minor x tick num = 1,
              ymin = 0, xmin = 5.5,
              ymax = 9000000, xmax = 8.5,
             % title = ,
              legend entries = {$Sony$, $Microsoft$, $Nintendo$}
          ]
           \addplot table [col sep=comma, x = Generation, y = Pixels]{Pixel_Sony.csv};
		   \addplot table [col sep=comma, x = Generation, y = Pixels]{Pixel_MS.csv};
		   \addplot table [col sep=comma, x = Generation, y = Pixels]{Pixel_Nintendo.csv};
       \end{axis}
    \end{tikzpicture}
    \caption{Maximale Auflösung Quelle: Eigene Darstellung}
    \label{abb:pixels}
\end{figure}


\begin{center}
        \csvautotabular{GPU_all.csv}
\end{center}



# Subplots

\begin{figure}[ht!]
\centering
	\begin{subfloat}
		\begin{tikzpicture}
		       \begin{axis}
		       	  [
		    	     width=\linewidth*.45, % Scale the plot to \linewidth
		              grid=major,
		              grid style={dashed,gray!30},
		              xlabel=Generation, % Set the labels
		              ylabel=GFLOPS,
		              legend pos = north west,
		              x tick label style = {rotate = 0, anchor=north, /pgf/number format/1000 sep= },
		              y tick label style = {/pgf/number format/1000 sep= },
		              xtick distance = 1,
		              minor x tick num = 1,
		              ymin = 0, xmin = 5.5,
		              ymax = 1900, xmax = 8.5,
		             % title = Entwicklung der Leistungsfähigkeit - hier: GFLOPS der GPU,
		              legend entries = {$Sony$, $Microsoft$, $Nintendo$}
		          ]
		           \addplot table [col sep=comma, x = Generation, y = GFLOPS]{GPU_Sony.csv};
				   \addplot table [col sep=comma, x = Generation, y = GFLOPS]{GPU_MS.csv};
				   \addplot table [col sep=comma, x = Generation, y = GFLOPS]{GPU_Nintendo.csv};
		       \end{axis}
		    \end{tikzpicture}
	\end{subfloat}
	\begin{subfloat}
		\begin{tikzpicture}
		       \begin{axis}
		       	  [
		    	     width=\linewidth*.45, % Scale the plot to \linewidth
		              grid=major,
		              grid style={dashed,gray!30},
		              xlabel=Generation, % Set the labels
		              ylabel=Pixels,
		              legend pos = north west,
		              x tick label style = {rotate = 0, anchor=north, /pgf/number format/1000 sep= },
		              y tick label style = {/pgf/number format/1000 sep= },
		              xtick distance = 1,
		              minor x tick num = 1,
		              ymin = 0, xmin = 5.5,
		              ymax = 9000000, xmax = 8.5,
		             % title = ,
		              legend entries = {$Sony$, $Microsoft$, $Nintendo$}
		          ]
		           \addplot table [col sep=comma, x = Generation, y = Pixels]{Pixel_Sony.csv};
				   \addplot table [col sep=comma, x = Generation, y = Pixels]{Pixel_MS.csv};
				   \addplot table [col sep=comma, x = Generation, y = Pixels]{Pixel_Nintendo.csv};
		       \end{axis}
		    \end{tikzpicture}
		\end{subfloat}
	    \caption{Performance increases over time (GPU power and max. resolution) / Source: THE INTERWEBS}
	    \label{fig:performance}
\end{figure}


# RAM diagram
\begin{tikzpicture}
       \begin{semilogyaxis}
       	  [
    	     width=\linewidth*.75, % Scale the plot to \linewidth
              grid=major,
              grid style={dashed,gray!30},
              xlabel=console generation, % Set the labels
              ylabel=RAM in bytes,
              legend pos = north west,
              x tick label style = {rotate = 0, anchor=north, /pgf/number format/1000 sep= },
              y tick label style = {/pgf/number format/1000 sep= },
              xtick distance = 1,
              minor x tick num = 1,
              ymin = 0, xmin = 2.5,
              ymax = , xmax = 8.5,
              legend entries = {$Sony$, $Microsoft$, $Nintendo$,$Sega$}
          ]
           \addplot table [col sep=comma, x = Generation, y = S_RAM]{ConsoleRam.csv};
		   \addplot table [col sep=comma, x = Generation, y = MS_RAM]{ConsoleRam.csv};
		   \addplot table [col sep=comma, x = Generation, y = N_RAM]{ConsoleRam.csv};
		   \addplot table [col sep=comma, x = Generation, y = SEG_RAM]{ConsoleRam.csv};
       \end{axis}
    \end{tikzpicture}

	\subsection{The Apple iPhone - a video games platform?}
	\label{the-apple-iphone---a-video-games-platform}
	%
	Notes:
	\begin{itemize}
	\item
	  is it a gaming platform? come up with numbers, e.g.~AppAnnie,
	  performance GeekBench \cite{AppAnnie2016}
	\item
	  apply framework and show why it´s a successful gaming system
	\item
	  simple controller design = direct input via touch
	\item
	  huge game quantity = variety
	\item
	  lot of high quality games = show via metacritic
	\item
	  discuss if there are any superstar software title
	\item
	  discuss Foster´s S Curve model and application to the
	  iPhone/Smartphone as a gaming device
	\item
	  Positioning
	\item
	  Hardware
	\item
	  Software
	\item
	  Value added digital services
	\item
	  It is an innovative, disrupting gaming platform
	\end{itemize}



%
\begin{itemize}
\tightlist
\item
  Multiplayer Gaming

  \begin{itemize}
  \tightlist
  \item
    direct network effects
  \end{itemize}
\item
  Social factors

  \begin{itemize}
  \tightlist
  \item
    social experiences
  \item
    flow in social gaming
  \item
    social interaction via chat, voice \cite{Trepte2012}
  \item
    self-expression (achievments, gamer score, \cite{Jakobsson2011}
  \end{itemize}
\item
  Digital game distribution

  \begin{itemize}
  \tightlist
  \item
    use for consumer: ease of consumption
  \item
    use for producer: direct sales, no logistics, vendor lock in -
    switching costs
  \end{itemize}
\item
  Technical requirements

  \begin{itemize}
  \tightlist
  \item
    stability
  \item
    responsiveness, lag free\cite{Ries2008}
  \item
    ease of use, User Interface Design
  \end{itemize}
\end{itemize}
%

%
\begin{itemize}
\tightlist
\item
  Positioning of the consoles, mainly via advertising, see if articles
  available
\item
  hardware specs
\item
  controller design - classification according proposed criteria
\item
  value added services - overview, target audience, issues?
\item
  sum up in table form
\item
  ``declare'' a winner
\end{itemize}

Nintendo Wii

\begin{itemize}
\tightlist
\item
  targetting/positioned for casual gamers and former non-gamers
\item
  controller design, motion control
\item
  reduced
\end{itemize}

Advertisement slogans:

\begin{itemize}
\tightlist
\item
  Wii move you
\item
  Wii would like to play (campaing with families, all ages, casual
  focus!)
\end{itemize}

Sony Playstation 3

Advertisement slogans:

\begin{itemize}
\tightlist
\item
  It only does everything
\item
  Long live play
\item
  The game is just the start
\item
  This is living
\item
  Dance with Zombies before blowing their heads off
\item
  Go beyond the game
\end{itemize}

Microsoft Xbox 360

Advertisement slogans:

\begin{itemize}
\tightlist
\item
  Jump in
\item
  The next generation now
\item
  Kinecting people
\end{itemize}



%
\begin{figure}
  \centering
    \begin{tabular}{l r r r r}
                & Games avl. & Consoles sold  & Total Games sold  & Attach rate \\ \hline
      Wii       & 2808       & 101.18M         & 965.72M            & 9.54\\
      PS3       & 3308       & 86.66M          & 969.01M            & 11.18\\
      XBox 360  & 3671       & 85.61M          & 1,001.63M          & 11.70\\ \hline
    \end{tabular}
    \caption{Generation 7 overview/ Source: \cite{Vgchartz.com2016}}
    \label{tab:gen7overview}
\end{figure}
%
\begin{figure}
  \centering
    \begin{tabular}{l r}
                & Superstars  \\ \hline
          Wii   & 34  \\
          PS3   & 160 \\
        XBox 360& 179 \\ \hline
    \end{tabular}
    \caption{Generation 7 superstar games with a ranking \textgreater 85\% / Source: \cite{Gamerankings2016}}
    \label{tab:gen7superstars}
\end{figure}
% 13 Wii Superstars are 1st party Nintendo titles
\begin{figure}
  \centering
    \begin{tabular}{l r r r}
                & \textgreater 5M  & 1st party & For Top100 \\ \hline
          Wii   & 21  & 66.7\%  & 1.48M  \\
          PS3   & 28  & 21.4\%  & 1.99M \\
        XBox 360& 28  & 42.9\%  & 2.20M  \\ \hline
    \end{tabular}
    \caption{Generation 7 beststeller games / Source: \cite{Vgchartz.com2016}}
    \label{tab:gen7beststeller}
\end{figure}
%
\begin{figure}
  \centering
  \begin{tabular}{l l l l}
                      & Wii       & PS3       & XBox 360     \\ \hline
      US launch price & 249.99 \$  & 599 \$     & 399.99 \$     \\
      EU launch price & \EUR{249} & \EUR{599} & \EUR{399.99} \\
      production costs& 160 \$     & 800 \$     & 715 \$        \\ \hline
      $\Delta$        & 89.99 \$   & -201 \$    & -315.01 \$     \\ \hline
  \end{tabular}
  \caption{Generation 7 launch pricing and estimated production costs / Source: !TODO}
  \label{tab:gen7launchpricingcosts}
\end{figure}
%
