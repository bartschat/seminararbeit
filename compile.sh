#!/bin/bash

# Translate the Markdown files into LaTex

#pandoc -f markdown -t latex ./Executive.md -o Executive.tex
#pandoc -f markdown -t latex ./Abkürzungen.md -o Abkürzungen.tex
#pandoc -f markdown -t latex ./Hauptteil.md -o Hauptteil.tex
#pandoc -f markdown -t latex ./Eidesstatt.md -o Eidesstatt.tex
#pandoc -f markdown -t latex ./Appendix.md -o Appendix.tex
# Concatenate all LaTeX files into one big file

#cat ./header.tex > output.tex
#cat ./Executive.tex >> output.tex
#cat ./Abbildungen.tex >> output.tex
#cat ./Abkürzungen.tex >> output.tex
#cat ./Inhaltsverzeichnis.tex >> output.tex
#cat ./Hauptteil.tex >> output.tex
#cat ./Literatur.tex >> output.tex
#cat ./Appendix.tex >> output.tex
#cat ./Eidesstatt.tex >> output.tex
#cat ./footer.tex >> output.tex

# Typeset the LaTeX thingie and build a nice pdf

#latexmk -gg -pdf ./output
pdflatex output
bibtex output
pdflatex output
# Check command line arguments and commit/push to git repo

if [ $# -eq 2 ]
        then
                if [ "$1" = "push" ]
                        then
                                /usr/bin/git add *
                                /usr/bin/git commit  -m "$2"
                                /usr/bin/git push
                        else
                                echo "If you want to push to git, please say so... "
                fi
	else
        	echo "Not pushed to git repo!"
fi
