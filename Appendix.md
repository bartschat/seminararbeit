  \newpage
  \begin{appendix}
  \section{Additional figures and tables}
  \label{app:figures}
  
  \begin{figure}
  \centering
  \begin{tikzpicture} 
         \begin{axis} 
         	  [
      	     width=\linewidth*.55, 
                grid=major, 
                grid style={dashed,gray!30},
                xlabel=Year, 
                ylabel= ,
                legend pos = north west,
                x tick label style = {rotate = 0, anchor=north, /pgf/number format/1000 sep= },
                y tick label style = {rotate = 0, /pgf/number format/1000 sep= },
                xtick distance = 5,
                minor x tick num = 1,
                ymin = 0, xmin = 1975,
                ymax = 13, xmax = 2006,
                legend entries = {$Buttons$, $Axis$}
            ]
             \addplot table [col sep=comma, x = Year, y = Buttons]{Controller_Buttons_Axis.csv};
             \addplot table [col sep=comma, x = Year, y = Axis]{Controller_Buttons_Axis.csv};
         \end{axis} 
      \end{tikzpicture}
      \caption{Rising complexity shown with the number of buttons and axis on control interfaces - Source: Own design based on photos provided by \cite{Brunner2013}}
      \label{fig:buttons}
\end{figure}
  
\begin{figure}
	\centering
	\small
	\begin{tabular}{ p{3.5cm} r p{3.5cm} r p{3.5cm} r}
PS4&&XBox One&&Wii U&\\ \hline
Title&Score&Title&Score&Title&Score\\ \hline
Angry Birds Star Wars&48.33&Angry Birds Star Wars&55&007 Legends&40.67\\
Assassin's Creed IV: Black Flag&85.31&Assassin's Creed IV: Black Flag&81&Assassin's Creed III&83\\
Battlefield 4&85&Battlefield 4&75&Batman: Arkham City - Armored Edition&84.87\\
Call of Duty: Ghosts&78.31&Call of Duty: Ghosts&77.4&Ben 10: Omniverse&30\\
Contrast&58.5&Crimson Dragon&56.11&Call of Duty: Black Ops II&86.07\\
DC Universe Online&72.5&Dead Rising 3&78.45&Chasing Aurora&65.38\\
Escape Plan&69.25&FIFA 14&89.92&Darksiders II&84.96\\
FIFA 14&87.92&Fighter Within&24.68&Epic Mickey 2: The Power of Two&55.42\\
Flower&93.57&Forza Motorsport 5&79.49&Family Party: 30 Great Games Obstacle Arcade&16.2\\
Injustice: Gods Among Us - Ultimate Edition&83.21&Just Dance 2014&63.33&FIFA 13&76.12\\
Just Dance 2014&75&Killer Instinct&74.5&Funky Barn&50.38\\
Killzone Shadow Fall&73.41&Lego Marvel Super Heroes&70.68&Game Party Champions&16.4\\
Knack&58.09&LocoCycle&51.38&Just Dance 4&67.64\\
Lego Marvel Super Heroes&83.24&Madden NFL 25&79.73&Little Inferno&81.8\\
Madden NFL 25&75.96&NBA 2K14&87.12&Mass Effect 3: Special Edition&86.22\\
NBA 2K14&84.18&NBA Live 14&35&Nano Assault Neo&71.83\\
NBA Live 14&46.8&Need for Speed Rivals&79.08&New Super Mario Bros. U&84.48\\
Need for Speed Rivals&80.59&Powerstar Golf&65.8&Nintendo Land&77.98\\
Putty Squad&53&Ryse: Son of Rome&64.3&Puddle&70.58\\
Resogun&85.45&Skylanders: Swap Force&80&Rabbids Land&52.4\\
Sound Shapes&82.67&Zoo Tycoon&70.1&Rise of the Guardians: The Video Game&47.5\\
Super Motherload&68.17&Zumba Fitness: World Party&74.67&Skylanders: Giants&78.17\\
The Playroom&&&&Sonic - All-Stars Racing Transformed&77.64\\
Trine 2: Complete Story&86.33&&&Sports Connection&29.29\\
Warframe&66.97&&&Tank! Tank! Tank!&49.31\\
War Thunder&74.3&&&Tekken Tag Tournament 2: Wii U Edition&83.15\\
&&&&Transformers: Prime – The Game&57.12\\
&&&&Trine 2: Director's Cut&86.33\\
&&&&Warriors Orochi 3 Hyper&66.54\\
&&&&Your Shape: Fitness Evolved 2013&77.14\\
&&&&ZombiU&77.25\\\hline
	\end{tabular}
	\caption{Launch lineups of the 8th console generation/ Source: Own design based on \cite{Gamerankings2016}}
	\label{tab:gen8launchlineup}
\end{figure}

\begin{figure}
	\centering
	\small
	\begin{tabular}{ l l l l}
					&PS4	&XBox One	&Wii U\\ \hline
	Median			&75.96	&74.59		&71.21\\
	Mean			&74.24	&68.76		&63.36\\
	Titles			&25		&2			&26\\
	\textgreater80	&5		&2			&6\\
	\textgreater85	&5		&2			&3\\
	\textless65		&3		&4			&4\\
	\textless50		&2		&2			&7\\ \hline
	\end{tabular}
	\caption{Launch lineups of the 8th console generation - statistics/ Source: Own design based on \cite{Gamerankings2016}}
	\label{tab:gen8launchlineupstats}
\end{figure}

\begin{figure}
	\centering
	\small
	\begin{tabular}{ l r}
	Title&Ranking \\ \hline
	Grand Theft Auto V&96.33\\ 
	The Last of Us Remastered&95.70\\ 
	Journey&94.80\\ 
	Uncharted 4: A Thief's End&92.71\\ 
	The Witcher 3: Wild Hunt&92.23\\ 
	Metal Gear Solid V: The Phantom Pain&91.59\\ 
	Diablo III: Ultimate Evil Edition&91.25\\ 
	The Witcher 3: Wild Hunt - Hearts of Stone&91.19\\ 
	Overwatch&90.68\\ 
	Bloodborne&90.66\\ 
	The Witcher 3: Wild Hunt - Blood and Wine&90.00\\ 
	Guacamelee! Super Turbo Championship Edition&89.79\\ 
	Dragon Age: Inquisition&89.68\\ 
	Titanfall 2&89.23\\ 
	Dark Souls III&88.93\\ 
	Fallout 4&88.60\\ 
	Rise of the Tomb Raider: 20 Year Celebration&88.58\\ 
	Batman: Arkham Knight&88.45\\ 
	The Talos Principle&88.06\\ 
	Divinity: Original Sin Enhanced Edition&	87.86\\ 
	Velocity 2X&87.50\\ 
	Dark Souls II: Scholar of the First Sin&87.23\\ 
	Rocket League&87.16\\ 
	Odin Sphere Leifthrasir&86.78\\ 
	Bloodborne: The Old Hunters&86.75\\ 
	Uncharted: The Nathan Drake Collection&86.66\\ 
	Valkyria Chronicles Remastered&86.61\\ 
	Rogue Legacy&86.60\\ 
	Pro Evolution Soccer 2016&86.58\\ 
	Middle-earth: Shadow of Mordor&86.55\\ 
	OlliOlli2: Welcome to Olliwood&86.46\\ 
	Ratchet \& Clank&86.27\\ 
	MLB The Show 16&86.19\\ 
	Final Fantasy XIV Online: A Realm Reborn&86.08\\ 
	Destiny: The Taken King&86.04\\ 
	DOOM&85.82\\ 
	Tomb Raider: Definitive Edition&85.76\\ 
	Resogun&85.45\\ 
	The Witness&85.31\\ 
	Thumper&85.26\\ 
	Pro Evolution Soccer 2017&85.18\\ 
	Guilty Gear Xrd -SIGN-&85.13\\ \hline
	\end{tabular}
	\caption{Superstar titles in generation 8 on PS4 (Rating over 85%)/ Source: Own design based on \cite{Gamerankings2016}}
	\label{tab:ps4superstars}
\end{figure}


\begin{figure}
	\centering
	\small
	\begin{tabular}{ l r}
	Title&Ranking \\ \hline
	Super Mario 3D World&92.56\\
	Super Smash Bros. for Wii U&92.39\\
	Bayonetta 2&91.38\\
	The Legend of Zelda: The Wind Waker HD& 91.08\\
	Shovel Knight&89.98\\
	Super Mario Maker&89.41\\
	Deus Ex: Human Revolution - Director´s Cut&89.30\\
	Mario Kart 8&88.40\\
	Pikmin 3&86.46\\
	Call of Duty: Black Ops II&86.07\\
	The Legend of Zelda: Twilight Princess HD&85.98\\\hline
	\end{tabular}
	\caption{Superstar titles in generation 8 on Wii U (Rating over 85%)/ Source: Own design based on \cite{Gamerankings2016}}
	\label{tab:wiiusuperstars}
\end{figure}


\end{appendix}